function [CR, b] = calcular_CR_B(segundos, acelerador, RPM)
CorteMin=min(acelerador);
CorteMax=max(acelerador);

aceleradorU = unique(acelerador(acelerador~=0));
aceleradorUF = aceleradorU(aceleradorU>=CorteMin & aceleradorU<=CorteMax);

for i = 1:length(aceleradorUF)% For all of the unique and filtered throttle settings:
    RPMUF = RPM(acelerador==aceleradorUF(i));  % get RPM values cooresponding to unique/filtered Throttle value
    RPMpromedios100(i,1) = mean(RPMUF); % Take the average RPM value from these filtered data points
end

[CR_b, S] = polyfit(aceleradorUF,RPMpromedios100,1); % Perfom a linear fit on data
CR = CR_b(1); % First entry in CR_b is CR value
b = CR_b(2);  % Next entry is b value

% Plot the linear relationship and display calculated CR and b values
figure
% Plot all of the average RPM values for each throttle, and also plot a
% line showing the linear fit generated.
plot(aceleradorUF,RPMpromedios100,'*',RPMpromedios100,polyval(CR_b,aceleradorUF))
xlabel('Throttle (%)','FontSize',12)
ylabel('RPM (rev/min)','FontSize',12)
title({['Linear Fit of RPM vs. Throttle Setting']; ... 
    ' ';['C_R: ' num2str(CR) ' (RPM/%), b: ' num2str(b) ' (RPM)']},'FontSize',14)
grid on
end