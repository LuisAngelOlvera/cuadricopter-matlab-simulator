function[]=graficas(resultados,angulos_cmd_trayectoria,scope_datos_trayectoria,Scope1_Comandos_Comportamiento,C)

if C==0
    Phi_cmd=angulos_cmd_trayectoria.signals(2).values;
    Theta_cmd=angulos_cmd_trayectoria.signals(1).values;
    Psi_cmd=scope_datos_trayectoria.signals(4).values;
    X_cmd=scope_datos_trayectoria.signals(1).values;  
    Y_cmd=scope_datos_trayectoria.signals(2).values;
    alt_cmd=scope_datos_trayectoria.signals(3).values;
    Phi_m=zeros(1,length(Phi_cmd));
for i=1:length(Phi_cmd)
    Phi_m(i)=Phi_cmd(i);
    Theta_m(i)=Theta_cmd(i);
    Psi_m(i)=Psi_cmd(i);
    X_m(i)=X_cmd(i);
    Y_m(i)=Y_cmd(i);
    alt_m(i)=alt_cmd(i);
end
    Phi_m;
    Theta_m;
    Psi_m;
    X_m;
    Y_m;
    alt_m;
else C>=2;
    Phi_m=Scope1_Comandos_Comportamiento.signals(1).values;
    Theta_m=Scope1_Comandos_Comportamiento.signals(2).values;
    Psi_m=Scope1_Comandos_Comportamiento.signals(3).values;
    alt_m=Scope1_Comandos_Comportamiento.signals(4).values;
end
T=resultados.time;
P=resultados.signals(1).values;
Q=resultados.signals(2).values;
R=resultados.signals(3).values;
Phi=resultados.signals(4).values;
Theta=resultados.signals(5).values;
Psi=resultados.signals(6).values;
U=resultados.signals(7).values;
V=resultados.signals(8).values;
W=resultados.signals(9).values;
X=resultados.signals(10).values;
Y=resultados.signals(11).values;
Z=resultados.signals(12).values;

% Gr�ficas ___________________________________________________________________
f1=figure;
subplot(4,3,1)
plot(T,P,'b')
xlabel('Tiempo (s)')
ylabel('Velocidad Angular (grad/seg)')
xlim([min(T) max(T)])
title('P')
grid on

subplot(4,3,2)
plot(T,Q,'r')
xlabel('Tiempo (s)')
ylabel('Velocidad Angular (grad/seg)')
xlim([min(T) max(T)])
title('Q')
grid on

subplot(4,3,3)
plot(T,R,'g')
xlabel('Tiempo (s)')
ylabel('Velocidad Angular (grad/seg)')
xlim([min(T) max(T)])
title('R')
grid on

subplot(4,3,4)
plot(T,Phi*180/pi,'b')
hold on
plot(T,Phi_m*180/pi,'k--')
hold off
xlabel('Tiempo (s)')
ylabel('Angulo (grad)')
xlim([min(T) max(T)])
title('Phi')
grid on

subplot(4,3,5)
plot(T,Theta*180/pi,'r')
hold on
plot(T,Theta_m*180/pi,'k--')
hold off
xlabel('Tiempo (s)')
ylabel('Angulo (grad)')
xlim([min(T) max(T)])
title('Theta')
grid on

subplot(4,3,6)
plot(T,Psi*180/pi,'g')
hold on
plot(T,Psi_m*180/pi,'k--')
hold off
xlabel('Tiempo (s)')
ylabel('Angulo (grad)')
xlim([min(T) max(T)])
title('Psi')
grid on

subplot(4,3,7)
plot(T,U,'b')
xlabel('Tiempo (s)')
ylabel('Velocidad (m/s)')
xlim([min(T) max(T)])
title('U')
grid on

subplot(4,3,8)
plot(T,V,'r')
xlabel('Tiempo (s)')
ylabel('Velocidad (m/s)')
xlim([min(T) max(T)])
title('V')
grid on

subplot(4,3,9)
plot(T,W,'g')
xlabel('Tiempo (s)')
ylabel('Velocidad (m/s)')
xlim([min(T) max(T)])
title('W')
grid on

subplot(4,3,10)
plot(T,X,'b')
if C==0
hold on
plot(T,X_m,'k--')
hold off
end
xlabel('Tiempo (s)')
ylabel('Posicion (m)')
xlim([min(T) max(T)])
title('X')
grid on

subplot(4,3,11)
plot(T,Y,'r')
if C==0
hold on
plot(T,Y_m,'k--')
hold off
end
xlabel('Tiempo (s)')
ylabel('Posicion (m)')
xlim([min(T) max(T)])
title('Y')
grid on

subplot(4,3,12)
plot(T,Z,'g')
hold on
plot(T,alt_m,'k--')
hold off
xlabel('Tiempo (s)')
ylabel('Posicion (m)')
xlim([min(T) max(T)])
title('Z')
grid on