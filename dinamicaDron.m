function dinamicaDron(block)
setup(block);
% Esta funci�n S-function, simula el comportamiento del drone basado en las
% datos de rendimiento de los par�metros y las entradas de las RPM de los
% motores


function setup(block)
    
  % �Qu� hace la funci�n Block:
% Configure las caracter�sticas b�sicas del bloque de funciones S, como:
% - Puertos de entrada
% - Puertos de salida
% - par�metros de di�logo
% - Opciones

    %% �Qu� es Simulink.BlockPreCompInputPortData? (INFORMACI�N DE ESTA SECCI�N OBTENIDA DEL HELP DE MATLAB)
        % Proporciona informaci�n de precompilaci�n sobre el puerto de entrada del bloque

        % DESCRIPCI�N
        % El software Simulink� devuelve una instancia de esta clase cuando un programa MATLAB�, 
        % por ejemplo, una funci�n S de MATLAB de nivel 2, invoca el m�todo InputPort del objeto en tiempo de ejecuci�n 
        % de un bloque antes de que se haya compilado el modelo que contiene el bloque.

        %PROIEDADES Y DESCRIPCI�N DE DirectFeedthrough

        %Verdadero si este puerto de entrada tiene alimentaci�n directa.
        %Tipo de datos: Booleano
        %Acceso: RW para funciones de MATLAB S, RO para otros bloques

    % �Qu� es Simulink.BlockPortData? (INFORMACI�N DE ESTA SECCI�N OBTENIDA DEL HELP DE MATLAB)
        % Describir el puerto de entrada o salida de bloque

        % SamplingMode,Descripci�n
        % Modo de muestreo del puerto. Los valores v�lidos son:

        % marco: El puerto acepta o env�a se�ales basadas en cuadros. El uso de se�ales basadas en cuadros requiere una licencia DSP System Toolbox �.
        % heredado: El modo de muestreo se hereda del puerto al que est� conectado este puerto.
        % muestra: El puerto acepta o muestra datos muestreados.

        % Tipo de datos: string

        % Acceso: RW para las funciones de MATLAB S, RO para otros bloques

     % �Qu� es Simulink.BlockData? (INFORMACI�N DE ESTA SECCI�N OBTENIDA DEL HELP DE MATLAB)
         % Proporciona informaci�n en tiempo de ejecuci�n sobre datos relacionados con bloques, como par�metros de bloque

         % DESCRIPCI�N: Esta clase define las propiedades que son comunes a los objetos que proporcionan informaci�n 
         % en tiempo de ejecuci�n sobre los puertos y vectores de trabajo de un bloque.
        %Dimensions
            %Descripci�n: Dimensions del objeto relacionado con el bloque, por ejemplo, par�metro o vector DWork.
            
            %Tipo de datos: Array
            % Acceso:  RW para funciones de MATLAB S, RO para otros bloques
                % Tipo, Descripci�n:
                % Tipo de datos de bloque. Los valores posibles son:
                % BlockPreCompInputPortData: Este objeto contiene datos para un puerto de entrada antes de compilar el modelo.
                % BlockPreCompOutputPortData: Este objeto contiene datos para un puerto de salida antes de compilar el modelo.
                % BlockCompInputPortData: Este objeto contiene datos para un puerto de entrada despu�s de compilar el modelo.
                % BlockCompOutputPortData: Este objeto contiene datos para un puerto de salida despu�s de compilar el modelo.
                % BlockPreCompDworkData: Este objeto contiene datos para un vector DWork antes de compilar el modelo.
                % BlockCompDworkData: Este objeto contiene datos para un vector DWork despu�s de compilar el modelo.
                % BlockDialogPrmData: Este objeto describe un par�metro de cuadro de di�logo de una funci�n S de MATLAB de nivel 2.
                % BlockRuntimePrmData: Este objeto describe un par�metro de tiempo de ejecuci�n de una funci�n S de MATLAB de nivel 2.
                % BlockCompContStatesData: Este objeto describe los estados continuos del bloque en el paso de tiempo actual.
                % BlockDerivativesData: Este objeto describe las derivadas de los estados continuos del bloque en el paso de tiempo actual.
                
                % Tipo de datos: String
                % Acceso:RO
                
% �Qu� es Simulink.RunTimeBlock? (INFORMACI�N DE ESTA SECCI�N OBTENIDA DEL HELP DE MATLAB)
    % Permite que la funci�n S de MATLAB de nivel 2 y otros programas MATLAB obtengan informaci�n sobre el bloque mientras se ejecuta la simulaci�n
    %DESCRIPCI�N: Esta clase permite que una funci�n S de MATLAB� de nivel 2 u otro programa MATLAB obtenga informaci�n sobre un bloque. 
        % El software Simulink� crea una instancia de esta clase o una clase derivada para cada bloque en un modelo. 
        % El software Simulink transfiere el objeto a los m�todos de devoluci�n de llamada de las funciones S de MATLAB...
        %...de nivel 2 cuando actualiza o simula un modelo, permitiendo que los m�todos de devoluci�n de llamada obtengan...
        %...informaci�n relacionada con el bloque y proporcionen dicha informaci�n al software Simulink.
        
        %CurrentTime: Tiempo de simulaci�n actual
        %NumDworks: N�mero de vectores de trabajo discretos utilizados por el bloque.
        %NumOutputPorts: N�mero de puertos de salida de bloque.
        %NumContStates: N�mero de estados continuos del bloque.
        %NumDworkDiscStates: N�mero de estados discretos del bloque
        %NumDialogPrms: N�mero de par�metros que pueden ingresarse en el cuadro de di�logo del bloque de funciones S.
        %NumInputPorts: N�mero de puertos de entrada del bloque.
        %NumRuntimePrms: N�mero de par�metros de tiempo de ejecuci�n utilizados por bloque.
        %SampleTimes: Tiempos de muestra en los que el bloque produce salidas.
    % �Qu� es Simulink.MSFcnRunTimeBlock? (INFORMACI�N DE ESTA SECCI�N OBTENIDA DEL HELP DE MATLAB)
        % Obtiene la informaci�n para correr una funci�n S-function para un
        % bloque de una funci�n MATLAB nivel 2

%%
  % Registra el n�mero de entradas y de salidas
  %------
  block.NumInputPorts  = 5;
  %------
  block.NumOutputPorts = 12;
  
% Configure las propiedades del puerto para ser heredado o din�mico.
  
  for i = 1:4; % Son las entradas que dan los motores (primeras 4 entradas, falta las perturbaciones)
  block.InputPort(i).Dimensions        = 1;
  block.InputPort(i).DirectFeedthrough = false;
  block.InputPort(i).SamplingMode      = 'Sample';
  end
  %------
  % Entrada de las perturbaciones
  block.InputPort(5).Dimensions        = 6; % torques x,y,z; fuerzas x,y,z.
  block.InputPort(5).DirectFeedthrough = false;
  block.InputPort(5).SamplingMode      = 'Sample';
  %------
  for i = 1:12;
  block.OutputPort(i).Dimensions       = 1;
  block.OutputPort(i).SamplingMode     = 'Sample';
  end
  % Registra los par�metros
  block.NumDialogPrms     = 2;
  
  % Configura los estados continuos
  block.NumContStates = 12;
  % Configura el tiempo de muestreo      
  block.SampleTimes = [0 0]; 
 %%
 % Especifica si el acelerador deber�a usar un TLC (Target Language Compiler)o llamar a una funci�n de MATLAB 
  
 % SimStateCompliance:Especifica el comportamiento de una S-function cuando
                     % est� guardando el SimState de un modelo que contiene la funci�n S
 % CheckParameters: Revisa la validaci�n de los par�metros de una S-Function de MATLAB 
 % InitializeConditions: Inicializa el vector de estado de una funci�n S en MATLAB
 % Outputs: Calcula las se�ales que emite un bloque de una funci�n S de MATLAB
 % Derivative: Da la salida de una derivada de una entrada, esta funci�n
 % calcula la derivada de la se�al de una entrada u con respecto al tiempo
 % de simulaci�n t
%%
  block.SetAccelRunOnTLC(false);
  block.SimStateCompliance = 'DefaultSimState';
  block.RegBlockMethod('CheckParameters', @CheckPrms);
  block.RegBlockMethod('InitializeConditions', @InitializeConditions);
  block.RegBlockMethod('Outputs', @Outputs);
  block.RegBlockMethod('Derivatives', @Derivatives);

 function CheckPrms(block)
     ModeloDron   = block.DialogPrm(1).Data;
     CI     = block.DialogPrm(2).Data;
function InitializeConditions(block)
% Inicia los 12 estados
CI = block.DialogPrm(2).Data;

% CI.P, CI.Q, CI.R son en grados/s
P = CI.P*pi/180; Q = CI.Q*pi/180; R = CI.R*pi/180;
% CI.Phi, CI.The, CI.Psi son en grados/s
Phi = CI.Phi*pi/180; The = CI.The*pi/180; Psi = CI.Psi*pi/180;
U = CI.U; V = CI.V; W = CI.W; 
X = CI.X; Y = CI.Y; Z = CI.Z;

init = [P,Q,R,Phi,The,Psi,U,V,W,X,Y,Z];
for i=1:12
block.OutputPort(i).Data = init(i);
block.ContStates.Data(i) = init(i);
end

function Outputs(block)
for i = 1:12;
  block.OutputPort(i).Data = block.ContStates.Data(i);

end


function Derivatives(block)
% Nombra todos las entradas de los motores y de los estados
ModeloDron = block.DialogPrm(1).Data;

% P Q R en grad/seg
P = block.ContStates.Data(1);
Q = block.ContStates.Data(2);
R = block.ContStates.Data(3);
% Phi The Psi en grados
Phi = block.ContStates.Data(4);
The = block.ContStates.Data(5);
Psi = block.ContStates.Data(6);
% U V W en m/s
U = block.ContStates.Data(7);
V = block.ContStates.Data(8);
W = block.ContStates.Data(9);
% X Y Z en m
X = block.ContStates.Data(10);
Y = block.ContStates.Data(11);
Z = block.ContStates.Data(12);
% w values en rev/min! NO EN RADIANES/SEG
w1 = block.InputPort(1).Data;
w2 = block.InputPort(2).Data;
w3 = block.InputPort(3).Data;
w4 = block.InputPort(4).Data;
w  = [w1; w2; w3; w4];
%------
Dist_tau = block.InputPort(5).Data(1:3);
Dist_F   = block.InputPort(5).Data(4:6);
%------

% Calcula las fuerzas y los momentos
% Momento total debido a la velocidad del motor
% Momentos deben estar en N*m
% La determinaci�n experimental de Ct ay Cq deber�an ser ajustadas al
% modelo usando Kg en vez de lb
% Mb = (modeloDron.dctcq*(w.^2)) + (Dist_tau);  %(dctcq*(w.^2)); % Mb = [tau1 tau2 tau3]'
ModeloDron.Jm=3.7882e-06; %Momento de Inercia del giroscopio
ModeloDron.dctcq=[0 ModeloDron.CT 0 -ModeloDron.CT;-ModeloDron.CT 0 ModeloDron.CT 0;-ModeloDron.CQ ModeloDron.CQ -ModeloDron.CQ ModeloDron.CQ];

tau_motorGyro = [Q*ModeloDron.Jm*2*pi/60*(-w1-w3+w2+w4); 
P*ModeloDron.Jm*2*pi/60*(w1+w3-w2-w4); 0];
 Mb = (ModeloDron.dctcq*(w.^2))+ tau_motorGyro + (Dist_tau);  % Mb = [tau1 tau2 tau3]'

% Empuje debido a la velocidad del motor
% Por simplicidad, las fuerzas deben ser manejadas en Newtons
% La aceleraci�n en la ecuaci�n de estado de la velocidad angular
Fb = [0; 0; sum(ModeloDron.CT*(w.^2))];   %[0, 0, sum(ct*w.^2)]'

% Obtener dP dQ dR
omb_bi = [P; Q; R];
OMb_bi = [ 0,-R, Q;
           R, 0,-P;
          -Q, P, 0];

b_omdotb_bi = ModeloDron.Jbinv*(Mb-OMb_bi*ModeloDron.Jb*omb_bi);
H_Phi = [1,tan(The)*sin(Phi), tan(The)*cos(Phi);
         0,         cos(Phi),         -sin(Phi);
         0,sin(Phi)/cos(The),cos(Phi)/cos(The)];   
Phidot = H_Phi*omb_bi;

% Calcular la matriz de rotaci�n
% Se usa una rotaci�n Z-Y-X
Rib = [cos(Psi)*cos(The) cos(Psi)*sin(The)*sin(Phi)-sin(Psi)*cos(Phi) cos(Psi)*sin(The)*cos(Phi)+sin(Psi)*sin(Phi);
       sin(Psi)*cos(The) sin(Psi)*sin(The)*sin(Phi)+cos(Psi)*cos(Phi) sin(Psi)*sin(The)*cos(Phi)-cos(Psi)*sin(Phi);
       -sin(The)         cos(The)*sin(Phi)                            cos(The)*cos(Phi)];
Rbi = Rib';
ge = [0; 0; -ModeloDron.g];
gb = Rbi*ge;
Dist_Fb = Rbi*Dist_F;

% Calcula las derivadas de la velocidad y posici�n en marco de cuerpo 
vb = [U;V;W];
b_dv = (1/ModeloDron.masa)*Fb+gb+Dist_Fb-OMb_bi*vb; % Aceleraci�n en marco del cuerpo (para velocidad)
i_dp = Rib*vb; % Velocidad en el marco inercial (para posici�n)

dP = b_omdotb_bi(1);
dQ = b_omdotb_bi(2);
dR = b_omdotb_bi(3);
dPhi = Phidot(1);
dTheta = Phidot(2);
dPsi = Phidot(3);
dU = b_dv(1);
dV = b_dv(2);
dW = b_dv(3);
dX = i_dp(1);
dY = i_dp(2);
dZ = i_dp(3);
% Para fines de validaci�n, no podemos tener una Z negativa, entonces
% siempre lo mantendremos en 0.
if ((Z<=0) && (dZ<=0)) % mejor versi�n que el anterior?
    dZ = 0;
    block.ContStates.Data(12) = 0;
end
f = [dP dQ dR dPhi dTheta dPsi dU dV dW dX dY dZ].';
  % Lo de arriba es  un vector de derivadas de los estados
block.Derivatives.Data = f;

