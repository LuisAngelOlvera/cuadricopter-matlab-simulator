function[f2]=iniciar_animacion(resultados,angulos_cmd_trayectoria,scope_datos_trayectoria,Scope1_Comandos_Comportamiento,C)
f2=figure(2);
if C==0
    Phi_cmd=angulos_cmd_trayectoria.signals(1).values;
    Theta_cmd=angulos_cmd_trayectoria.signals(1).values;
    Psi_cmd=scope_datos_trayectoria.signals(4).values;
    X_cmd=scope_datos_trayectoria.signals(1).values;  
    Y_cmd=scope_datos_trayectoria.signals(2).values;
    alt_cmd=scope_datos_trayectoria.signals(3).values;
    Phi_m=zeros(1,length(Phi_cmd));
for i=1:length(Phi_cmd)
    Phi_m(i)=Phi_cmd(i);
    Theta_m(i)=Theta_cmd(i);
    Psi_m(i)=Psi_cmd(i);
    X_m(i)=X_cmd(i);
    Y_m(i)=Y_cmd(i);
    alt_m(i)=alt_cmd(i);
end
    Phi_m;
    Theta_m;
    Psi_m;
    X_m;
    Y_m;
    alt_m;
else C>=2;
    Phi_m=Scope1_Comandos_Comportamiento.signals(1).values;
    Theta_m=Scope1_Comandos_Comportamiento.signals(2).values;
    Psi_m=Scope1_Comandos_Comportamiento.signals(3).values;
    alt_m=Scope1_Comandos_Comportamiento.signals(4).values;
end
T=resultados.time;
P=resultados.signals(1).values;
Q=resultados.signals(2).values;
R=resultados.signals(3).values;
Phi=resultados.signals(4).values;
Theta=resultados.signals(5).values;
Psi=resultados.signals(6).values;
U=resultados.signals(7).values;
V=resultados.signals(8).values;
W=resultados.signals(9).values;
X=resultados.signals(10).values;
Y=resultados.signals(11).values;
Z=resultados.signals(12).values;
%% Animación
N=length(X);
X_cmd=scope_datos_trayectoria.signals(1).values;  
Y_cmd=scope_datos_trayectoria.signals(2).values;
alt_cmd=scope_datos_trayectoria.signals(3).values;

for i=1:length(Phi_cmd)
    X1(i)=X_cmd(i);
    Y1(i)=Y_cmd(i);
    Z1(i)=alt_cmd(i);
end
    X1;
    Y1;
    Z1;
for i=1:N
%     clf
    hold on
    plot3(X(i),Y(i),Z(i),'ro')
    plot3(X1,Y1,Z1,'k')
    grid on
    axis tight
    
    title(sprintf('(%f, %f)',X1(i),Y1(i), Z1(i)))
    xlabel('X metros')
    ylabel('Y metros')
    zlabel('Z metros')
    
    drawnow
end