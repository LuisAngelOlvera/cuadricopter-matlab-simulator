% Generación de trayectoria DIAMANTE
X=[0 0 0 0 0 0 0 0 0 1.5 3 0 0 -1.5 -3]; % metros
Y=[0 0 0 0 0 0 0 1.5 3 0 0 -1.5 -3 0 0]; % metros
Z=[0 1 3 6 6 9 9 12    12  12    12   12   12    12  12]; % metros
t=[0 5 7.5 10 12.5 15 17.5 20 22.5 25 27.5 30 32.5 35 37.5]; % segundos
angulo=[0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]; % radians
path.x = timeseries(X,t);
path.y = timeseries(Y,t);
path.z = timeseries(Z,t);
path.psi = timeseries(angulo,t);
