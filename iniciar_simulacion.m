function [varargout] = iniciar_simulacion(varargin)

% INICIAR_SIMULACION MATLAB code for iniciar_simulacion.fig
%      INICIAR_SIMULACION, by itself, creates a new INICIAR_SIMULACION or raises the existing
%      singleton*.
%
%      H = INICIAR_SIMULACION returns the handle to a new INICIAR_SIMULACION or the handle to
%      the existing singleton*.
%
%      INICIAR_SIMULACION('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in INICIAR_SIMULACION.M with the given input arguments.
%
%      INICIAR_SIMULACION('Property','Value',...) creates a new INICIAR_SIMULACION or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before iniciar_simulacion_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to iniciar_simulacion_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help iniciar_simulacion

% Last Modified by GUIDE v2.5 24-Nov-2019 18:01:07

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @iniciar_simulacion_OpeningFcn, ...
                   'gui_OutputFcn',  @iniciar_simulacion_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before iniciar_simulacion is made visible.
function iniciar_simulacion_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to iniciar_simulacion (see VARARGIN)

% Choose default command line output for iniciar_simulacion
handles.output = hObject;


% Update handles structure
guidata(hObject, handles);

initialize_gui(hObject, handles, false);

% UIWAIT makes iniciar_simulacion wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = iniciar_simulacion_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in CargarDatosExcel.
function CargarDatosExcel_Callback(hObject, eventdata, handles)
% hObject    handle to CargarDatosExcel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
axes(handles.axes2);
DronImagen = imread('DronImagen.jpg');
imshow(DronImagen);

axes(handles.axes3);
UNIVA_Imagen = imread('UNIVA.JPG');
imshow(UNIVA_Imagen);

[archivo,ruta] = uigetfile({'*.xlsx';'*.mat';'*.*'},...
                          'File Selector');
disp('Trabajando... :)')
Excel_Archivo=archivo;
segundos = xlsread(Excel_Archivo,'A:A');
acelerador = xlsread(Excel_Archivo,'B:B');
RPM = xlsread(Excel_Archivo,'C:C');
medicion_gramos = xlsread(Excel_Archivo,'D:D');

DEM = struct('segundos',(segundos),'acelerador',(acelerador),'RPM',(RPM),'medicion_gramos',(medicion_gramos));

     assignin('base','DEM',DEM);

disp('Listo... :)')
guidata(hObject, handles);


% --- Executes on button press in GuardarDatosMat.
function GuardarDatosMat_Callback(hObject, eventdata, handles)
% hObject    handle to GuardarDatosMat (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[archivo,ruta] = uigetfile({'*.xlsx';'*.mat';'*.*'},...
                          'File Selector');
disp('Trabajando... :)')
Excel_Archivo=archivo;
segundos = xlsread(Excel_Archivo,'A:A');
acelerador = xlsread(Excel_Archivo,'B:B');
RPM = xlsread(Excel_Archivo,'C:C');
medicion_gramos = xlsread(Excel_Archivo,'D:D');
    assignin('base','segundos',segundos);
    assignin('base','acelerador',acelerador);
    assignin('base','RPM',RPM);
    assignin('base','medicion_gramos',medicion_gramos);
%
%PROCESO DE ALMACENAMIENTO
DEM = struct('segundos',(segundos),'acelerador',(acelerador),'RPM',(RPM),'medicion_gramos',(medicion_gramos));

assignin('base','DEM',DEM);
% Saves the structure "quadModel" into the working directory
disp('Listo... :)')
uisave('DEM','Dato_Experimental_Motor')

% --- Executes on button press in CalcularCoeficiente.
function CalcularCoeficiente_Callback(hObject, eventdata, handles)
% hObject    handle to CalcularCoeficiente (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
segundos = evalin('base','DEM.segundos');
acelerador = evalin('base','DEM.acelerador');
RPM = evalin('base','DEM.RPM');
medicion_gramos = evalin('base','DEM.medicion_gramos');

[CQ] = calcular_CQ(segundos, acelerador, RPM, medicion_gramos);
       assignin('base','CQ',CQ);

[CR, b] = calcular_CR_B(segundos, acelerador, RPM);
CRp = [CR, b];
        assignin('base','CR',CR);
        assignin('base','b',b);
   
[CT] = calcular_CT(segundos, acelerador, RPM, medicion_gramos);
       assignin('base','CT',CT);
 
CQ_DEM = CQ;
CR_DEM = CR;
b_DEM = b;
CT_DEM = CT;
set(handles.motor_CQ_valor, 'String', CQ_DEM);
set(handles.motor_CR_valor, 'String', CR_DEM);
set(handles.motor_b_valor, 'String', b_DEM);
set(handles.motor_CT_valor, 'String', CT_DEM);


function gravedad_editar_total_Callback(hObject, eventdata, handles)
% hObject    handle to gravedad_editar_total (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of gravedad_editar_total as text
%        str2double(get(hObject,'String')) returns contents of gravedad_editar_total as a double
gravedad_editar_total = str2double(get(hObject, 'String'));
if isnan(gravedad_editar_total)
    set(hObject, 'String', 0);
    errordlg('Entrada deber ser un n�mero real','Error');
end

% Guarda el valor...
handles.metricdata.gravedad_editar_total= gravedad_editar_total;
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function gravedad_editar_total_CreateFcn(hObject, eventdata, handles)
% hObject    handle to gravedad_editar_total (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function motor_CQ_editar_final_Callback(hObject, eventdata, handles)
% hObject    handle to motor_CQ_editar_final (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of motor_CQ_editar_final as text
%        str2double(get(hObject,'String')) returns contents of motor_CQ_editar_final as a double
motor_CQ_editar_final = str2double(get(hObject, 'String'));
if isnan(motor_CQ_editar_final)
    set(hObject, 'String', 0);
    errordlg('Entrada deber ser un n�mero real','Error');
end

% Guarda el valor...
handles.metricdata.motor_CQ_editar_final= motor_CQ_editar_final;
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function motor_CQ_editar_final_CreateFcn(hObject, eventdata, handles)
% hObject    handle to motor_CQ_editar_final (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function motor_CR_editar_final_Callback(hObject, eventdata, handles)
% hObject    handle to motor_CR_editar_final (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of motor_CR_editar_final as text
%        str2double(get(hObject,'String')) returns contents of motor_CR_editar_final as a double
motor_CR_editar_final = str2double(get(hObject, 'String'));
if isnan(motor_CR_editar_final)
    set(hObject, 'String', 0);
    errordlg('Entrada deber ser un n�mero real','Error');
end

% Guarda el valor...
handles.metricdata.motor_CR_editar_final= motor_CR_editar_final;
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function motor_CR_editar_final_CreateFcn(hObject, eventdata, handles)
% hObject    handle to motor_CR_editar_final (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function motor_b_editar_final_Callback(hObject, eventdata, handles)
% hObject    handle to motor_b_editar_final (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of motor_b_editar_final as text
%        str2double(get(hObject,'String')) returns contents of motor_b_editar_final as a double
motor_b_editar_final= str2double(get(hObject, 'String'));
if isnan(motor_b_editar_final)
    set(hObject, 'String', 0);
    errordlg('Entrada deber ser un n�mero real','Error');
end

% Guarda el valor...
handles.metricdata.motor_b_editar_final= motor_b_editar_final;
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function motor_b_editar_final_CreateFcn(hObject, eventdata, handles)
% hObject    handle to motor_b_editar_final (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function motor_CT_editar_final_Callback(hObject, eventdata, handles)
% hObject    handle to motor_CT_editar_final (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of motor_CT_editar_final as text
%        str2double(get(hObject,'String')) returns contents of motor_CT_editar_final as a double
motor_CT_editar_final= str2double(get(hObject, 'String'));
if isnan(motor_CT_editar_final)
    set(hObject, 'String', 0);
    errordlg('Entrada deber ser un n�mero real','Error');
end

% Guarda el valor...
handles.metricdata.motor_CT_editar_final= motor_CT_editar_final;
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function motor_CT_editar_final_CreateFcn(hObject, eventdata, handles)
% hObject    handle to motor_CT_editar_final (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function brazos_editar_m_Callback(hObject, eventdata, handles)
% hObject    handle to brazos_editar_m (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of brazos_editar_m as text
%        str2double(get(hObject,'String')) returns contents of brazos_editar_m as a double
brazos_editar_m= str2double(get(hObject, 'String'));
if isnan(brazos_editar_m)
    set(hObject, 'String', 0);
    errordlg('Entrada deber ser un n�mero real','Error');
end

% Guarda el valor...
handles.metricdata.brazos_editar_m= brazos_editar_m;
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function brazos_editar_m_CreateFcn(hObject, eventdata, handles)
% hObject    handle to brazos_editar_m (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function brazos_editar_r_Callback(hObject, eventdata, handles)
% hObject    handle to brazos_editar_r (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of brazos_editar_r as text
%        str2double(get(hObject,'String')) returns contents of brazos_editar_r as a double
brazos_editar_r= str2double(get(hObject, 'String'));
if isnan(brazos_editar_r)
    set(hObject, 'String', 0);
    errordlg('Entrada deber ser un n�mero real','Error');
end

% Guarda el valor...
handles.metricdata.brazos_editar_r= brazos_editar_r;
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function brazos_editar_r_CreateFcn(hObject, eventdata, handles)
% hObject    handle to brazos_editar_r (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function brazos_editar_L_Callback(hObject, eventdata, handles)
% hObject    handle to brazos_editar_L (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of brazos_editar_L as text
%        str2double(get(hObject,'String')) returns contents of brazos_editar_L as a double
brazos_editar_L= str2double(get(hObject, 'String'));
if isnan(brazos_editar_L)
    set(hObject, 'String', 0);
    errordlg('Entrada deber ser un n�mero real','Error');
end

% Guarda el valor...
handles.metricdata.brazos_editar_L= brazos_editar_L;
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function brazos_editar_L_CreateFcn(hObject, eventdata, handles)
% hObject    handle to brazos_editar_L (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function brazos_editar_da_Callback(hObject, eventdata, handles)
% hObject    handle to brazos_editar_da (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of brazos_editar_da as text
%        str2double(get(hObject,'String')) returns contents of brazos_editar_da as a double
brazos_editar_da= str2double(get(hObject, 'String'));
if isnan(brazos_editar_da)
    set(hObject, 'String', 0);
    errordlg('Entrada deber ser un n�mero real','Error');
end

% Guarda el valor...
handles.metricdata.brazos_editar_da= brazos_editar_da;
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function brazos_editar_da_CreateFcn(hObject, eventdata, handles)
% hObject    handle to brazos_editar_da (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function PCD_editar_m_Callback(hObject, eventdata, handles)
% hObject    handle to PCD_editar_m (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of PCD_editar_m as text
%        str2double(get(hObject,'String')) returns contents of PCD_editar_m as a double
PCD_editar_m= str2double(get(hObject, 'String'));
if isnan(PCD_editar_m)
    set(hObject, 'String', 0);
    errordlg('Entrada deber ser un n�mero real','Error');
end

% Guarda el valor...
handles.metricdata.PCD_editar_m= PCD_editar_m;
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function PCD_editar_m_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PCD_editar_m (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function PCD_editar_r_Callback(hObject, eventdata, handles)
% hObject    handle to PCD_editar_r (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of PCD_editar_r as text
%        str2double(get(hObject,'String')) returns contents of PCD_editar_r as a double
PCD_editar_r= str2double(get(hObject, 'String'));
if isnan(PCD_editar_r)
    set(hObject, 'String', 0);
    errordlg('Entrada deber ser un n�mero real','Error');
end

% Guarda el valor...
handles.metricdata.PCD_editar_r= PCD_editar_r;
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function PCD_editar_r_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PCD_editar_r (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function PCD_editar_h_Callback(hObject, eventdata, handles)
% hObject    handle to PCD_editar_h (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of PCD_editar_h as text
%        str2double(get(hObject,'String')) returns contents of PCD_editar_h as a double
PCD_editar_h= str2double(get(hObject, 'String'));
if isnan(PCD_editar_h)
    set(hObject, 'String', 0);
    errordlg('Entrada deber ser un n�mero real','Error');
end

% Guarda el valor...
handles.metricdata.PCD_editar_h= PCD_editar_h;
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function PCD_editar_h_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PCD_editar_h (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ESC_editar_m_Callback(hObject, eventdata, handles)
% hObject    handle to ESC_editar_m (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ESC_editar_m as text
%        str2double(get(hObject,'String')) returns contents of ESC_editar_m as a double
ESC_editar_m= str2double(get(hObject, 'String'));
if isnan(ESC_editar_m)
    set(hObject, 'String', 0);
    errordlg('Entrada deber ser un n�mero real','Error');
end

% Guarda el valor...
handles.metricdata.ESC_editar_m= ESC_editar_m;
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function ESC_editar_m_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ESC_editar_m (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ESC_editar_a_Callback(hObject, eventdata, handles)
% hObject    handle to ESC_editar_a (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ESC_editar_a as text
%        str2double(get(hObject,'String')) returns contents of ESC_editar_a as a double
ESC_editar_a= str2double(get(hObject, 'String'));
if isnan(ESC_editar_a)
    set(hObject, 'String', 0);
    errordlg('Entrada deber ser un n�mero real','Error');
end

% Guarda el valor...
handles.metricdata.ESC_editar_a= ESC_editar_a;
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function ESC_editar_a_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ESC_editar_a (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ESC_editar_b_Callback(hObject, eventdata, handles)
% hObject    handle to ESC_editar_b (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ESC_editar_b as text
%        str2double(get(hObject,'String')) returns contents of ESC_editar_b as a double
ESC_editar_b= str2double(get(hObject, 'String'));
if isnan(ESC_editar_b)
    set(hObject, 'String', 0);
    errordlg('Entrada deber ser un n�mero real','Error');
end

% Guarda el valor...
handles.metricdata.ESC_editar_b= ESC_editar_b;
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function ESC_editar_b_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ESC_editar_b (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function ESC_editar_ds_Callback(hObject, eventdata, handles)
% hObject    handle to ESC_editar_ds (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of ESC_editar_ds as text
%        str2double(get(hObject,'String')) returns contents of ESC_editar_ds as a double
ESC_editar_ds= str2double(get(hObject, 'String'));
if isnan(ESC_editar_ds)
    set(hObject, 'String', 0);
    errordlg('Entrada deber ser un n�mero real','Error');
end

% Guarda el valor...
handles.metricdata.ESC_editar_ds= ESC_editar_ds;
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function ESC_editar_ds_CreateFcn(hObject, eventdata, handles)
% hObject    handle to ESC_editar_ds (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function masa_editar_total_Callback(hObject, eventdata, handles)
% hObject    handle to masa_editar_total (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of masa_editar_total as text
%        str2double(get(hObject,'String')) returns contents of masa_editar_total as a double
masa_editar_total= str2double(get(hObject, 'String'));
if isnan(masa_editar_total)
    set(hObject, 'String', 0);
    errordlg('Entrada deber ser un n�mero real','Error');
end

% Guarda el valor...
handles.metricdata.masa_editar_total= masa_editar_total;
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function masa_editar_total_CreateFcn(hObject, eventdata, handles)
% hObject    handle to masa_editar_total (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function motores_editar_m_Callback(hObject, eventdata, handles)
% hObject    handle to motores_editar_m (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of motores_editar_m as text
%        str2double(get(hObject,'String')) returns contents of motores_editar_m as a double
motores_editar_m = str2double(get(hObject, 'String'));
if isnan(motores_editar_m)
    set(hObject, 'String', 0);
    errordlg('Entrada deber ser un n�mero real','Error');
end

% Guarda el valor...
handles.metricdata.motores_editar_m= motores_editar_m;
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function motores_editar_m_CreateFcn(hObject, eventdata, handles)
% hObject    handle to motores_editar_m (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function motores_editar_dm_Callback(hObject, eventdata, handles)
% hObject    handle to motores_editar_dm (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of motores_editar_dm as text
%        str2double(get(hObject,'String')) returns contents of motores_editar_dm as a double
motores_editar_dm = str2double(get(hObject, 'String'));
if isnan(motores_editar_dm)
    set(hObject, 'String', 0);
    errordlg('Entrada deber ser un n�mero real','Error');
end

% Guarda el valor...
handles.metricdata.motores_editar_dm= motores_editar_dm;
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function motores_editar_dm_CreateFcn(hObject, eventdata, handles)
% hObject    handle to motores_editar_dm (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function motores_editar_h_Callback(hObject, eventdata, handles)
% hObject    handle to motores_editar_h (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of motores_editar_h as text
%        str2double(get(hObject,'String')) returns contents of motores_editar_h as a double
motores_editar_h = str2double(get(hObject, 'String'));
if isnan(motores_editar_h)
    set(hObject, 'String', 0);
    errordlg('Entrada deber ser un n�mero real','Error');
end

% Guarda el valor...
handles.metricdata.motores_editar_h= motores_editar_h;
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function motores_editar_h_CreateFcn(hObject, eventdata, handles)
% hObject    handle to motores_editar_h (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function motores_editar_r_Callback(hObject, eventdata, handles)
% hObject    handle to motores_editar_r (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of motores_editar_r as text
%        str2double(get(hObject,'String')) returns contents of motores_editar_r as a double
motores_editar_r = str2double(get(hObject, 'String'));
if isnan(motores_editar_r)
    set(hObject, 'String', 0);
    errordlg('Entrada deber ser un n�mero real','Error');
end

% Guarda el valor...
handles.metricdata.motores_editar_r= motores_editar_r;
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function motores_editar_r_CreateFcn(hObject, eventdata, handles)
% hObject    handle to motores_editar_r (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function gravedad_editar_g_Callback(hObject, eventdata, handles)
% hObject    handle to gravedad_editar_g (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of gravedad_editar_g as text
%        str2double(get(hObject,'String')) returns contents of gravedad_editar_g as a double
gravedad_editar_g = str2double(get(hObject, 'String'));
if isnan(gravedad_editar_g)
    set(hObject, 'String', 0);
    errordlg('Entrada deber ser un n�mero real','Error');
end

% Guarda el valor...
handles.metricdata.gravedad_editar_g= gravedad_editar_g;
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function gravedad_editar_g_CreateFcn(hObject, eventdata, handles)
% hObject    handle to gravedad_editar_g (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function masa_editar_m_Callback(hObject, eventdata, handles)
% hObject    handle to masa_editar_m (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of masa_editar_m as text
%        str2double(get(hObject,'String')) returns contents of masa_editar_m as a double
masa_editar_m = str2double(get(hObject, 'String'));
if isnan(masa_editar_m)
    set(hObject, 'String', 0);
    errordlg('Entrada deber ser un n�mero real','Error');
end

% Guarda el valor...
handles.metricdata.masa_editar_m=masa_editar_m;
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function masa_editar_m_CreateFcn(hObject, eventdata, handles)
% hObject    handle to masa_editar_m (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function VelocidadAngular_editar_R_Callback(hObject, eventdata, handles)
% hObject    handle to VelocidadAngular_editar_R (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of VelocidadAngular_editar_R as text
%        str2double(get(hObject,'String')) returns contents of VelocidadAngular_editar_R as a double
VelocidadAngular_editar_R = str2double(get(hObject, 'String'));
if isnan(VelocidadAngular_editar_R)
    set(hObject, 'String', 0);
    errordlg('Entrada deber ser un n�mero real','Error');
end

% Guarda el valor...
handles.metricdata.VelocidadAngular_editar_R=VelocidadAngular_editar_R;
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function VelocidadAngular_editar_R_CreateFcn(hObject, eventdata, handles)
% hObject    handle to VelocidadAngular_editar_R (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function VelocidadAngular_editar_Q_Callback(hObject, eventdata, handles)
% hObject    handle to VelocidadAngular_editar_Q (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of VelocidadAngular_editar_Q as text
%        str2double(get(hObject,'String')) returns contents of VelocidadAngular_editar_Q as a double
VelocidadAngular_editar_Q = str2double(get(hObject, 'String'));
if isnan(VelocidadAngular_editar_Q)
    set(hObject, 'String', 0);
    errordlg('Entrada deber ser un n�mero real','Error');
end

% Guarda el valor...
handles.metricdata.VelocidadAngular_editar_Q=VelocidadAngular_editar_Q;
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function VelocidadAngular_editar_Q_CreateFcn(hObject, eventdata, handles)
% hObject    handle to VelocidadAngular_editar_Q (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function VelocidadAngular_editar_P_Callback(hObject, eventdata, handles)
% hObject    handle to VelocidadAngular_editar_P (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of VelocidadAngular_editar_P as text
%        str2double(get(hObject,'String')) returns contents of VelocidadAngular_editar_P as a double
VelocidadAngular_editar_P = str2double(get(hObject, 'String'));
if isnan(VelocidadAngular_editar_P)
    set(hObject, 'String', 0);
    errordlg('Entrada deber ser un n�mero real','Error');
end

% Guarda el valor...
handles.metricdata.VelocidadAngular_editar_P=VelocidadAngular_editar_P;
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function VelocidadAngular_editar_P_CreateFcn(hObject, eventdata, handles)
% hObject    handle to VelocidadAngular_editar_P (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit34_Callback(hObject, eventdata, handles)
% hObject    handle to VelocidadAngular_editar_P (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of VelocidadAngular_editar_P as text
%        str2double(get(hObject,'String')) returns contents of VelocidadAngular_editar_P as a double


% --- Executes during object creation, after setting all properties.
function edit34_CreateFcn(hObject, eventdata, handles)
% hObject    handle to VelocidadAngular_editar_P (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit35_Callback(hObject, eventdata, handles)
% hObject    handle to VelocidadAngular_editar_Q (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of VelocidadAngular_editar_Q as text
%        str2double(get(hObject,'String')) returns contents of VelocidadAngular_editar_Q as a double


% --- Executes during object creation, after setting all properties.
function edit35_CreateFcn(hObject, eventdata, handles)
% hObject    handle to VelocidadAngular_editar_Q (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit36_Callback(hObject, eventdata, handles)
% hObject    handle to VelocidadAngular_editar_R (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of VelocidadAngular_editar_R as text
%        str2double(get(hObject,'String')) returns contents of VelocidadAngular_editar_R as a double


% --- Executes during object creation, after setting all properties.
function edit36_CreateFcn(hObject, eventdata, handles)
% hObject    handle to VelocidadAngular_editar_R (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit39_Callback(hObject, eventdata, handles)
% hObject    handle to edit39 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit39 as text
%        str2double(get(hObject,'String')) returns contents of edit39 as a double


% --- Executes during object creation, after setting all properties.
function edit39_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit39 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit38_Callback(hObject, eventdata, handles)
% hObject    handle to edit38 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit38 as text
%        str2double(get(hObject,'String')) returns contents of edit38 as a double


% --- Executes during object creation, after setting all properties.
function edit38_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit38 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit37_Callback(hObject, eventdata, handles)
% hObject    handle to edit37 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit37 as text
%        str2double(get(hObject,'String')) returns contents of edit37 as a double


% --- Executes during object creation, after setting all properties.
function edit37_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit37 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function AnguloEuler_editar_phi_Callback(hObject, eventdata, handles)
% hObject    handle to AnguloEuler_editar_phi (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of AnguloEuler_editar_phi as text
%        str2double(get(hObject,'String')) returns contents of AnguloEuler_editar_phi as a double
AnguloEuler_editar_phi = str2double(get(hObject, 'String'));
if isnan(AnguloEuler_editar_phi)
    set(hObject, 'String', 0);
    errordlg('Entrada deber ser un n�mero real','Error');
end

% Guarda el valor...
handles.metricdata.AnguloEuler_editar_phi= AnguloEuler_editar_phi;
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function AnguloEuler_editar_phi_CreateFcn(hObject, eventdata, handles)
% hObject    handle to AnguloEuler_editar_phi (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function AnguloEuler_editar_the_Callback(hObject, eventdata, handles)
% hObject    handle to AnguloEuler_editar_the (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of AnguloEuler_editar_the as text
%        str2double(get(hObject,'String')) returns contents of AnguloEuler_editar_the as a double
AnguloEuler_editar_the = str2double(get(hObject, 'String'));
if isnan(AnguloEuler_editar_the)
    set(hObject, 'String', 0);
    errordlg('Entrada deber ser un n�mero real','Error');
end

% Guarda el valor...
handles.metricdata.AnguloEuler_editar_the= AnguloEuler_editar_the;
guidata(hObject,handles)


% --- Executes during object creation, after setting all properties.
function AnguloEuler_editar_the_CreateFcn(hObject, eventdata, handles)
% hObject    handle to AnguloEuler_editar_the (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function AnguloEuler_editar_psi_Callback(hObject, eventdata, handles)
% hObject    handle to AnguloEuler_editar_psi (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of AnguloEuler_editar_psi as text
%        str2double(get(hObject,'String')) returns contents of AnguloEuler_editar_psi as a double
AnguloEuler_editar_psi = str2double(get(hObject, 'String'));
if isnan(AnguloEuler_editar_psi)
    set(hObject, 'String', 0);
    errordlg('Entrada deber ser un n�mero real','Error');
end

% Guarda el valor...
handles.metricdata.AnguloEuler_editar_psi= AnguloEuler_editar_psi;
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function AnguloEuler_editar_psi_CreateFcn(hObject, eventdata, handles)
% hObject    handle to AnguloEuler_editar_psi (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function posicion_editar_X_Callback(hObject, eventdata, handles)
% hObject    handle to posicion_editar_X (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of posicion_editar_X as text
%        str2double(get(hObject,'String')) returns contents of posicion_editar_X as a double
posicion_editar_X = str2double(get(hObject, 'String'));
if isnan(posicion_editar_X)
    set(hObject, 'String', 0);
    errordlg('Entrada deber ser un n�mero real','Error');
end

% Guarda el valor...
handles.metricdata.posicion_editar_X= posicion_editar_X;
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function posicion_editar_X_CreateFcn(hObject, eventdata, handles)
% hObject    handle to posicion_editar_X (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function posicion_editar_Y_Callback(hObject, eventdata, handles)
% hObject    handle to posicion_editar_Y (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of posicion_editar_Y as text
%        str2double(get(hObject,'String')) returns contents of posicion_editar_Y as a double
posicion_editar_Y = str2double(get(hObject, 'String'));
if isnan(posicion_editar_Y)
    set(hObject, 'String', 0);
    errordlg('Entrada deber ser un n�mero real','Error');
end

% Guarda el valor...
handles.metricdata.posicion_editar_Y= posicion_editar_Y;
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function posicion_editar_Y_CreateFcn(hObject, eventdata, handles)
% hObject    handle to posicion_editar_Y (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function posicion_editar_Z_Callback(hObject, eventdata, handles)
% hObject    handle to posicion_editar_Z (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of posicion_editar_Z as text
%        str2double(get(hObject,'String')) returns contents of posicion_editar_Z as a double
posicion_editar_Z = str2double(get(hObject, 'String'));
if isnan(posicion_editar_Z)
    set(hObject, 'String', 0);
    errordlg('Entrada deber ser un n�mero real','Error');
end

% Guarda el valor...
handles.metricdata.posicion_editar_Z= posicion_editar_Z;
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function posicion_editar_Z_CreateFcn(hObject, eventdata, handles)
% hObject    handle to posicion_editar_Z (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function VelocidadLineal_editar_U_Callback(hObject, eventdata, handles)
% hObject    handle to VelocidadLineal_editar_U (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of VelocidadLineal_editar_U as text
%        str2double(get(hObject,'String')) returns contents of VelocidadLineal_editar_U as a double
VelocidadLineal_editar_U = str2double(get(hObject, 'String'));
if isnan(VelocidadLineal_editar_U)
    set(hObject, 'String', 0);
    errordlg('Entrada deber ser un n�mero real','Error');
end

% Guarda el valor...
handles.metricdata.VelocidadLineal_editar_U= VelocidadLineal_editar_U;
guidata(hObject,handles)


% --- Executes during object creation, after setting all properties.
function VelocidadLineal_editar_U_CreateFcn(hObject, eventdata, handles)
% hObject    handle to VelocidadLineal_editar_U (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function VelocidadLineal_editar_V_Callback(hObject, eventdata, handles)
% hObject    handle to VelocidadLineal_editar_V (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of VelocidadLineal_editar_V as text
%        str2double(get(hObject,'String')) returns contents of VelocidadLineal_editar_V as a double
VelocidadLineal_editar_V = str2double(get(hObject, 'String'));
if isnan(VelocidadLineal_editar_V)
    set(hObject, 'String', 0);
    errordlg('Entrada deber ser un n�mero real','Error');
end

% Guarda el valor...
handles.metricdata.VelocidadLineal_editar_V= VelocidadLineal_editar_V;
guidata(hObject,handles)


% --- Executes during object creation, after setting all properties.
function VelocidadLineal_editar_V_CreateFcn(hObject, eventdata, handles)
% hObject    handle to VelocidadLineal_editar_V (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function VelocidadLineal_editar_W_Callback(hObject, eventdata, handles)
% hObject    handle to VelocidadLineal_editar_W (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of VelocidadLineal_editar_W as text
%        str2double(get(hObject,'String')) returns contents of VelocidadLineal_editar_W as a double
VelocidadLineal_editar_W = str2double(get(hObject, 'String'));
if isnan(VelocidadLineal_editar_W)
    set(hObject, 'String', 0);
    errordlg('Entrada deber ser un n�mero real','Error');
end

% Guarda el valor...
handles.metricdata.VelocidadLineal_editar_W= VelocidadLineal_editar_W;
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function VelocidadLineal_editar_W_CreateFcn(hObject, eventdata, handles)
% hObject    handle to VelocidadLineal_editar_W (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function VelocidadMotor_editar_w1_Callback(hObject, eventdata, handles)
% hObject    handle to VelocidadMotor_editar_w1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of VelocidadMotor_editar_w1 as text
%        str2double(get(hObject,'String')) returns contents of VelocidadMotor_editar_w1 as a double
VelocidadMotor_editar_w1 = str2double(get(hObject, 'String'));
if isnan(VelocidadMotor_editar_w1)
    set(hObject, 'String', 0);
    errordlg('Entrada deber ser un n�mero real','Error');
end

% Guarda el valor...
handles.metricdata.VelocidadMotor_editar_w1= VelocidadMotor_editar_w1;
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function VelocidadMotor_editar_w1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to VelocidadMotor_editar_w1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function VelocidadMotor_editar_w2_Callback(hObject, eventdata, handles)
% hObject    handle to VelocidadMotor_editar_w2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of VelocidadMotor_editar_w2 as text
%        str2double(get(hObject,'String')) returns contents of VelocidadMotor_editar_w2 as a double
VelocidadMotor_editar_w2 = str2double(get(hObject, 'String'));
if isnan(VelocidadMotor_editar_w2)
    set(hObject, 'String', 0);
    errordlg('Entrada deber ser un n�mero real','Error');
end

% Guarda el valor...
handles.metricdata.VelocidadMotor_editar_w2= VelocidadMotor_editar_w2;
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function VelocidadMotor_editar_w2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to VelocidadMotor_editar_w2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function VelocidadMotor_editar_w3_Callback(hObject, eventdata, handles)
% hObject    handle to VelocidadMotor_editar_w3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of VelocidadMotor_editar_w3 as text
%        str2double(get(hObject,'String')) returns contents of VelocidadMotor_editar_w3 as a double
VelocidadMotor_editar_w3 = str2double(get(hObject, 'String'));
if isnan(VelocidadMotor_editar_w3)
    set(hObject, 'String', 0);
    errordlg('Entrada deber ser un n�mero real','Error');
end

% Guarda el valor...
handles.metricdata.VelocidadMotor_editar_w3= VelocidadMotor_editar_w3;
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function VelocidadMotor_editar_w3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to VelocidadMotor_editar_w3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function VelocidadMotor_editar_w4_Callback(hObject, eventdata, handles)
% hObject    handle to VelocidadMotor_editar_w4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of VelocidadMotor_editar_w4 as text
%        str2double(get(hObject,'String')) returns contents of VelocidadMotor_editar_w4 as a double
VelocidadMotor_editar_w4 = str2double(get(hObject, 'String'));
if isnan(VelocidadMotor_editar_w4)
    set(hObject, 'String', 0);
    errordlg('Entrada deber ser un n�mero real','Error');
end

% Guarda el valor...
handles.metricdata.VelocidadMotor_editar_w4= VelocidadMotor_editar_w4;
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function VelocidadMotor_editar_w4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to VelocidadMotor_editar_w4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



function edit62_Callback(hObject, eventdata, handles)
% hObject    handle to edit62 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit62 as text
%        str2double(get(hObject,'String')) returns contents of edit62 as a double


% --- Executes during object creation, after setting all properties.
function edit62_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit62 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit63_Callback(hObject, eventdata, handles)
% hObject    handle to edit63 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit63 as text
%        str2double(get(hObject,'String')) returns contents of edit63 as a double


% --- Executes during object creation, after setting all properties.
function edit63_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit63 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit64_Callback(hObject, eventdata, handles)
% hObject    handle to edit64 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit64 as text
%        str2double(get(hObject,'String')) returns contents of edit64 as a double


% --- Executes during object creation, after setting all properties.
function edit64_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit64 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit59_Callback(hObject, eventdata, handles)
% hObject    handle to edit59 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit59 as text
%        str2double(get(hObject,'String')) returns contents of edit59 as a double


% --- Executes during object creation, after setting all properties.
function edit59_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit59 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit60_Callback(hObject, eventdata, handles)
% hObject    handle to edit60 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit60 as text
%        str2double(get(hObject,'String')) returns contents of edit60 as a double


% --- Executes during object creation, after setting all properties.
function edit60_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit60 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit61_Callback(hObject, eventdata, handles)
% hObject    handle to edit61 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit61 as text
%        str2double(get(hObject,'String')) returns contents of edit61 as a double


% --- Executes during object creation, after setting all properties.
function edit61_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit61 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit56_Callback(hObject, eventdata, handles)
% hObject    handle to edit56 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit56 as text
%        str2double(get(hObject,'String')) returns contents of edit56 as a double


% --- Executes during object creation, after setting all properties.
function edit56_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit56 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit57_Callback(hObject, eventdata, handles)
% hObject    handle to edit57 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit57 as text
%        str2double(get(hObject,'String')) returns contents of edit57 as a double


% --- Executes during object creation, after setting all properties.
function edit57_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit57 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit58_Callback(hObject, eventdata, handles)
% hObject    handle to edit58 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit58 as text
%        str2double(get(hObject,'String')) returns contents of edit58 as a double


% --- Executes during object creation, after setting all properties.
function edit58_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit58 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function PID_editar_Roll_kp_Callback(hObject, eventdata, handles)
% hObject    handle to PID_editar_Roll_kp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of PID_editar_Roll_kp as text
%        str2double(get(hObject,'String')) returns contents of PID_editar_Roll_kp as a double
PID_editar_Roll_kp = str2double(get(hObject, 'String'));
if isnan(PID_editar_Roll_kp)
    set(hObject, 'String', 0);
    errordlg('Entrada deber ser un n�mero real','Error');
end

% Guarda el valor...
handles.metricdata.PID_editar_Roll_kp= PID_editar_Roll_kp;
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function PID_editar_Roll_kp_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PID_editar_Roll_kp (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function PID_editar_Roll_kip_Callback(hObject, eventdata, handles)
% hObject    handle to PID_editar_Roll_kip (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of PID_editar_Roll_kip as text
%        str2double(get(hObject,'String')) returns contents of PID_editar_Roll_kip as a double
PID_editar_Roll_kip = str2double(get(hObject, 'String'));
if isnan(PID_editar_Roll_kip)
    set(hObject, 'String', 0);
    errordlg('Entrada deber ser un n�mero real','Error');
end

% Guarda el valor...
handles.metricdata.PID_editar_Roll_kip= PID_editar_Roll_kip;
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function PID_editar_Roll_kip_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PID_editar_Roll_kip (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function PID_editar_Roll_kd_Callback(hObject, eventdata, handles)
% hObject    handle to PID_editar_Roll_kd (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of PID_editar_Roll_kd as text
%        str2double(get(hObject,'String')) returns contents of PID_editar_Roll_kd as a double
PID_editar_Roll_kd = str2double(get(hObject, 'String'));
if isnan(PID_editar_Roll_kd)
    set(hObject, 'String', 0);
    errordlg('Entrada deber ser un n�mero real','Error');
end

% Guarda el valor...
handles.metricdata.PID_editar_Roll_kd= PID_editar_Roll_kd;
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function PID_editar_Roll_kd_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PID_editar_Roll_kd (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit73_Callback(hObject, eventdata, handles)
% hObject    handle to edit73 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit73 as text
%        str2double(get(hObject,'String')) returns contents of edit73 as a double


% --- Executes during object creation, after setting all properties.
function edit73_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit73 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit72_Callback(hObject, eventdata, handles)
% hObject    handle to edit72 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit72 as text
%        str2double(get(hObject,'String')) returns contents of edit72 as a double


% --- Executes during object creation, after setting all properties.
function edit72_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit72 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit71_Callback(hObject, eventdata, handles)
% hObject    handle to edit71 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit71 as text
%        str2double(get(hObject,'String')) returns contents of edit71 as a double


% --- Executes during object creation, after setting all properties.
function edit71_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit71 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function PID_editar_yaw_kds_Callback(hObject, eventdata, handles)
% hObject    handle to PID_editar_yaw_kds (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of PID_editar_yaw_kds as text
%        str2double(get(hObject,'String')) returns contents of PID_editar_yaw_kds as a double
PID_editar_yaw_kds = str2double(get(hObject, 'String'));
if isnan(PID_editar_yaw_kds)
    set(hObject, 'String', 0);
    errordlg('Entrada deber ser un n�mero real','Error');
end

% Guarda el valor...
handles.metricdata.PID_editar_yaw_kds= PID_editar_yaw_kds;
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function PID_editar_yaw_kds_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PID_editar_yaw_kds (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function PID_editar_yaw_kps_Callback(hObject, eventdata, handles)
% hObject    handle to PID_editar_yaw_kps (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of PID_editar_yaw_kps as text
%        str2double(get(hObject,'String')) returns contents of PID_editar_yaw_kps as a double
PID_editar_yaw_kps = str2double(get(hObject, 'String'));
if isnan(PID_editar_yaw_kps)
    set(hObject, 'String', 0);
    errordlg('Entrada deber ser un n�mero real','Error');
end

% Guarda el valor...
handles.metricdata.PID_editar_yaw_kps= PID_editar_yaw_kps;
guidata(hObject,handles)


% --- Executes during object creation, after setting all properties.
function PID_editar_yaw_kps_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PID_editar_yaw_kps (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function PID_editar_yaw_kis_Callback(hObject, eventdata, handles)
% hObject    handle to PID_editar_yaw_kis (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of PID_editar_yaw_kis as text
%        str2double(get(hObject,'String')) returns contents of PID_editar_yaw_kis as a double
PID_editar_yaw_kis = str2double(get(hObject, 'String'));
if isnan(PID_editar_yaw_kis)
    set(hObject, 'String', 0);
    errordlg('Entrada deber ser un n�mero real','Error');
end

% Guarda el valor...
handles.metricdata.PID_editar_yaw_kis= PID_editar_yaw_kis;
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function PID_editar_yaw_kis_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PID_editar_yaw_kis (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function PID_editar_Pitch_kpt_Callback(hObject, eventdata, handles)
% hObject    handle to PID_editar_Pitch_kpt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of PID_editar_Pitch_kpt as text
%        str2double(get(hObject,'String')) returns contents of PID_editar_Pitch_kpt as a double
PID_editar_Pitch_kpt = str2double(get(hObject, 'String'));
if isnan(PID_editar_Pitch_kpt)
    set(hObject, 'String', 0);
    errordlg('Entrada deber ser un n�mero real','Error');
end

% Guarda el valor...
handles.metricdata.PID_editar_Pitch_kpt= PID_editar_Pitch_kpt;
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function PID_editar_Pitch_kpt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PID_editar_Pitch_kpt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function PID_editar_Pitch_kit_Callback(hObject, eventdata, handles)
% hObject    handle to PID_editar_Pitch_kit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of PID_editar_Pitch_kit as text
%        str2double(get(hObject,'String')) returns contents of PID_editar_Pitch_kit as a double
PID_editar_Pitch_kit = str2double(get(hObject, 'String'));
if isnan(PID_editar_Pitch_kit)
    set(hObject, 'String', 0);
    errordlg('Entrada deber ser un n�mero real','Error');
end

% Guarda el valor...
handles.metricdata.PID_editar_Pitch_kit= PID_editar_Pitch_kit;
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function PID_editar_Pitch_kit_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PID_editar_Pitch_kit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function PID_editar_Pitch_kdt_Callback(hObject, eventdata, handles)
% hObject    handle to PID_editar_Pitch_kdt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of PID_editar_Pitch_kdt as text
%        str2double(get(hObject,'String')) returns contents of PID_editar_Pitch_kdt as a double
PID_editar_Pitch_kdt = str2double(get(hObject, 'String'));
if isnan(PID_editar_Pitch_kdt)
    set(hObject, 'String', 0);
    errordlg('Entrada deber ser un n�mero real','Error');
end

% Guarda el valor...
handles.metricdata.PID_editar_Pitch_kdt= PID_editar_Pitch_kdt;
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function PID_editar_Pitch_kdt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PID_editar_Pitch_kdt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function PID_editar_z_kpz_Callback(hObject, eventdata, handles)
% hObject    handle to PID_editar_z_kpz (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of PID_editar_z_kpz as text
%        str2double(get(hObject,'String')) returns contents of PID_editar_z_kpz as a double
PID_editar_Pitch_z = str2double(get(hObject, 'String'));
if isnan(PID_editar_Pitch_z)
    set(hObject, 'String', 0);
    errordlg('Entrada deber ser un n�mero real','Error');
end

% Guarda el valor...
handles.metricdata.PID_editar_Pitch_z= PID_editar_Pitch_z;
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function PID_editar_z_kpz_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PID_editar_z_kpz (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function PID_editar_z_kiz_Callback(hObject, eventdata, handles)
% hObject    handle to PID_editar_z_kiz (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of PID_editar_z_kiz as text
%        str2double(get(hObject,'String')) returns contents of PID_editar_z_kiz as a double
PID_editar_z_kiz = str2double(get(hObject, 'String'));
if isnan(PID_editar_z_kiz)
    set(hObject, 'String', 0);
    errordlg('Entrada deber ser un n�mero real','Error');
end

% Guarda el valor...
handles.metricdata.PID_editar_z_kiz= PID_editar_z_kiz;
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function PID_editar_z_kiz_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PID_editar_z_kiz (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function PID_editar_z_kdz_Callback(hObject, eventdata, handles)
% hObject    handle to PID_editar_z_kdz (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of PID_editar_z_kdz as text
%        str2double(get(hObject,'String')) returns contents of PID_editar_z_kdz as a double
PID_editar_z_kdz = str2double(get(hObject, 'String'));
if isnan(PID_editar_z_kdz)
    set(hObject, 'String', 0);
    errordlg('Entrada deber ser un n�mero real','Error');
end

% Guarda el valor...
handles.metricdata.PID_editar_z_kdz= PID_editar_z_kdz;
guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function PID_editar_z_kdz_CreateFcn(hObject, eventdata, handles)
% hObject    handle to PID_editar_z_kdz (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in exportar_datos_workspace.
function exportar_datos_workspace_Callback(hObject, eventdata, handles)
% hObject    handle to exportar_datos_workspace (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
axes(handles.axes2);
DronImagen = imread('DronImagen.jpg');
imshow(DronImagen);

axes(handles.axes3);
UNIVA_Imagen = imread('UNIVA.JPG');
imshow(UNIVA_Imagen);
%% ESTANDARIZAR DATOS A UN SISTEMA DE MEDICI�N
%% Motores interpretaci�n de entrada
motor_m_g = str2num(get(handles.motores_editar_m, 'String'));
motor_dm_cm = str2num(get(handles.motores_editar_dm, 'String'));
motor_h_cm = str2num(get(handles.motores_editar_h, 'String'));
motor_r_cm = str2num(get(handles.motores_editar_r, 'String'));

%% ESC interpretaci�n de entrada
ESC_m_g = str2num(get(handles.ESC_editar_m, 'String'));
ESC_a_cm = str2num(get(handles.ESC_editar_a, 'String'));
ESC_b_cm = str2num(get(handles.ESC_editar_b, 'String'));
ESC_ds_cm = str2num(get(handles.ESC_editar_ds, 'String'));

%% Parte Central del Drone interpretaci�n de entrada
PCD_m_g = str2num(get(handles.PCD_editar_m, 'String'));
PCD_r_cm = str2num(get(handles.PCD_editar_r, 'String'));
PCD_H_cm = str2num(get(handles.PCD_editar_h, 'String'));

%% Brazos interpretaci�n de entrada
brazos_m_g = str2num(get(handles.brazos_editar_m, 'String'));
brazos_r_cm = str2num(get(handles.brazos_editar_r, 'String'));
brazos_L_cm = str2num(get(handles.brazos_editar_L, 'String'));
brazos_da_cm = str2num(get(handles.brazos_editar_da, 'String'));
%% Masa Total
masa = str2num(get(handles.masa_editar_m, 'String'));
%% Conversi�n al sistem Internacional de Medidas 
% Motores
motor_m = motor_m_g/1000;
motor_dm = motor_dm_cm/100;
motor_h = motor_h_cm/100;
motor_r = motor_r_cm/100;

% ESC
ESC_m = ESC_m_g/1000;
ESC_a = ESC_a_cm/100;
ESC_b = ESC_b_cm/100;
ESC_ds = ESC_ds_cm/100;

% PCD
PCD_m = PCD_m_g/1000;
PCD_r = PCD_r_cm/100;
PCD_H = PCD_H_cm/100;

% Brazos
brazos_m = brazos_m_g/1000;
brazos_r = brazos_r_cm/100;
brazos_L = brazos_L_cm/100;
brazos_da = brazos_da_cm/100;
% Masa Total
masa_k = masa/1000;
%% Momentos de inercia
% Este programa calcula los momentos de inercia del drone
%% Momentos de inercia Motores
% Jx1, Jy1, Jz1
Jx1 = (motor_m) * ((motor_r)^2) + (4/3)*(motor_m) * ((motor_h)^2) + 2*(motor_m)*((motor_dm)^2);
Jy1 = (motor_m) * ((motor_r)^2) + (4/3)*(motor_m) * ((motor_h)^2) + 2*(motor_m)*((motor_dm)^2);
Jz1 = 2*(motor_m)*((motor_r)^2) + (4)*(motor_m)*((motor_dm)^2);
%% Momentos de inercia ESC
% Jx2, Jy2, Jz2 
Jx2 = ((ESC_m*(ESC_a^2))/6) + ((ESC_m*(ESC_b^2))/6) + (2*(ESC_m)*(ESC_ds^2));
Jy2 = ((ESC_m*(ESC_a^2))/6) + ((ESC_m*(ESC_b^2))/6) + (2*(ESC_m)*(ESC_ds^2));
Jz2 = ((ESC_m*(ESC_a^2 + ESC_b^2))/3) + 4*(ESC_m)*(ESC_ds^2);
%% Momentos de Inercia Parte Central del Drone
% Jx3, Jy3, Jz3 
Jx3 = (1/4)*((PCD_m))*((PCD_r)^2) + (1/12)*((PCD_m))*((PCD_H)^2);
Jy3 = (1/4)*((PCD_m))*((PCD_r)^2) + (1/12)*((PCD_m))*((PCD_H)^2);
Jz3 = (1/2)*((PCD_m))*((PCD_r)^2);
%% Momentos de inercia Brazos
% Jx4, Jy4, Jz4
Jx4 = ((3/2)*(brazos_m)*(brazos_r^2)) + ((2/3)*(brazos_m)*(brazos_L^2)) + (2)*(brazos_m)*(brazos_da^2);
Jy4 = ((3/2)*(brazos_m)*(brazos_r^2)) + ((2/3)*(brazos_m)*(brazos_L^2)) + (2)*(brazos_m)*(brazos_da^2);
Jz4 = ((brazos_m)*((brazos_r)^2)) + (4/3)*((brazos_m)*((brazos_L)^2)) + (4)*(brazos_m)*(brazos_da^2);
%% Momentos de Inercia del drone completo
Jx = Jx1 + Jx2 + Jx3 + Jx4;
Jy = Jy1 + Jy2 + Jy3 + Jy4;
Jz = Jz1 + Jz2 + Jz3 + Jz4;
%Jb
Jb = [Jx 0 0; 0 Jy 0; 0 0 Jz];
Jbinv = [1/Jx 0 0; 0 1/Jy 0; 0 0 1/Jz];

%% Velocidad Angular
P = str2num(get(handles.VelocidadAngular_editar_P, 'String'));
Q = str2num(get(handles.VelocidadAngular_editar_Q, 'String'));
R = str2num(get(handles.VelocidadAngular_editar_R, 'String'));
%% �ngulos Euler
Phi = str2num(get(handles.AnguloEuler_editar_phi, 'String'));
The = str2num(get(handles.AnguloEuler_editar_the, 'String'));
Psi = str2num(get(handles.AnguloEuler_editar_psi, 'String'));
%% Velocidad Lineal
U = str2num(get(handles.VelocidadLineal_editar_U, 'String'));
V = str2num(get(handles.VelocidadLineal_editar_V, 'String'));
W = str2num(get(handles.VelocidadLineal_editar_W, 'String'));
%% Posici�n con Respecto a la Tierra
X = str2num(get(handles.posicion_editar_X, 'String'));
Y = str2num(get(handles.posicion_editar_Y, 'String'));
Z = str2num(get(handles.posicion_editar_Z, 'String'));
%% Velocidad de los motores
w1 = str2num(get(handles.VelocidadMotor_editar_w1, 'String'));
w2 = str2num(get(handles.VelocidadMotor_editar_w2, 'String'));
w3 = str2num(get(handles.VelocidadMotor_editar_w3, 'String'));
w4 = str2num(get(handles.VelocidadMotor_editar_w4, 'String'));
%% Coeficientes del motor
CQ = str2num(get(handles.motor_CQ_valor, 'String'));
CR = str2num(get(handles.motor_CR_valor, 'String'));
b = str2num(get(handles.motor_b_valor, 'String'));
CT = str2num(get(handles.motor_CT_valor, 'String'));

g=str2num(get(handles.gravedad_editar_g, 'String'));
% dctq = str2num(get(handles.dctq_editar, 'String'));

% PID roll
roll_kp=str2num(get(handles.PID_editar_Roll_kp, 'String'));
roll_kip=str2num(get(handles.PID_editar_Roll_kip, 'String'));
roll_kd=str2num(get(handles.PID_editar_Roll_kd, 'String'));

% PID pitch
pitch_kpt=str2num(get(handles.PID_editar_Pitch_kpt, 'String'));
pitch_kit=str2num(get(handles.PID_editar_Pitch_kit, 'String'));
pitch_kdt=str2num(get(handles.PID_editar_Pitch_kdt, 'String'));

% PID yaw
yaw_kps=str2num(get(handles.PID_editar_yaw_kps, 'String'));
yaw_kis=str2num(get(handles.PID_editar_yaw_kis, 'String'));
yaw_kds=str2num(get(handles.PID_editar_yaw_kds, 'String'));

% PID Z
z_kps=str2num(get(handles.PID_editar_z_kpz, 'String'));
z_kiz=str2num(get(handles.PID_editar_z_kiz, 'String'));
z_kdz=str2num(get(handles.PID_editar_z_kdz, 'String'));

    ModeloDron = struct('motor_m',(motor_m),'motor_dm',(motor_dm),'motor_h',(motor_h),'motor_r',(motor_r),...
'ESC_m',(ESC_m),'ESC_a',(ESC_a),'ESC_b',(ESC_b),'ESC_ds',(ESC_ds),...
'PCD_m',(PCD_m),'PCD_r',(PCD_r),'PCD_H',(PCD_H),...
'brazos_m',(brazos_m),'brazos_r',(brazos_r),'brazos_L',(brazos_L),'brazos_da',(brazos_da),'masa',(masa_k),...
'Jx',(Jx),'Jy',(Jy),'Jz',(Jz),'Jb',(Jb),'Jbinv',(Jbinv),...
'CQ',(CQ),'CR',(CR),'b',(b),'CT',(CT),'g',(g));


CI = struct('P',(P),'Q',(Q),'R',(R),...
    'Phi',(Phi),'The',(The),'Psi',(Psi),...
    'U',(U),'V',(V),'W',(W),...
    'X',(X),'Y',(Y),'Z',(Z),...
    'w1',(w1),'w2',(w2),'w3',(w3),'w4',(w4));

PID =struct('roll_kp',(roll_kp),'roll_kip',(roll_kip),'roll_kd',(roll_kd),...
'pitch_kpt',(pitch_kpt),'pitch_kit',(pitch_kit),'pitch_kdt',(pitch_kdt),...
'yaw_kps',(yaw_kps),'yaw_kis',(yaw_kis),'yaw_kds',(yaw_kds),...
'z_kps',(z_kps),'z_kiz',(z_kiz),'z_kdz',(z_kdz));

assignin('base','ModeloDron',ModeloDron);
assignin('base','CI',CI);
assignin('base','PID',PID);

guidata(hObject, handles);

% --- Executes on button press in datos_precargados.
function datos_precargados_Callback(hObject, eventdata, handles)
% hObject    handle to datos_precargados (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clc
evalin( 'base', 'clear variables' )
texto_datos_precargados_valor=('Datos en Workspace han sido borrados');
set(handles.texto_datos_precargados, 'String', texto_datos_precargados_valor);

% --- Executes on button press in resetar_valores.
function resetar_valores_Callback(hObject, eventdata, handles)
% hObject    handle to resetar_valores (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
initialize_gui(gcbf, handles, true);
texto_resetear_valores=('Datos en Workspace han sido borrados');
set(handles.texto_resetear_valores, 'String', texto_resetear_valores);

% --- Executes on button press in simulacion_trayectoria.
function simulacion_trayectoria_Callback(hObject, eventdata, handles)
% hObject    handle to simulacion_trayectoria (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of simulacion_trayectoria
if hObject == handles.simulacion_dinamica
    C=0;
assignin('base','C',C);
else
    C=0;
assignin('base','C',C);
end

% --- Executes on button press in simulacion_dinamica.
function simulacion_dinamica_Callback(hObject, eventdata, handles)
% hObject    handle to simulacion_dinamica (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of simulacion_dinamica
if hObject == handles.simulacion_trayectoria
    C=2;
X=[0 0  3  0  0   3 0]; % metros
Y=[0 0  0  3  0   0 0]; % metros
Z=[0 3  6  9 12 12 12]; % metros
t=[0 5 10 15 20 25 30]; % segundos
angulo=[0,90*pi/180, 0,180*pi/180, 0,  90*pi/180,  0]; % radians
path.x = timeseries(X,t);
path.y = timeseries(Y,t);
path.z = timeseries(Z,t);
path.psi = timeseries(angulo,t);
else
    C=4;
X=[0 0  0  0  0  0  0]; % metros
Y=[0 0  0  0  0  0  0]; % metros
Z=[0 0  0  0  0  0  0]; % metros
t=[0 0  0  0  0  0  0]; % segundos
angulo=[0,0,0,0, 0,0,0]; % radians
path.x = timeseries(X,t);
path.y = timeseries(Y,t);
path.z = timeseries(Z,t);
path.psi = timeseries(angulo,t);
assignin('base','C',C);
assignin('base','path',path);
end

% --- Executes on button press in cargar_trayectoria.
function cargar_trayectoria_Callback(hObject, eventdata, handles)
% hObject    handle to cargar_trayectoria (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[trayectoria,localizacion] = uigetfile({'*.*','Seleccionar archivo'})
ruta=trayectoria;
run(ruta)
x=timeseries(X,t);
y=timeseries(Y,t);
z=timeseries(Z,t);
psi=timeseries(angulo,t);
path=struct('x',x,'y',y,'z',z,'psi',psi);
assignin('base','path',path);
% assignin('base','path.y',path.y);
% assignin('base','path.z',path.z);
% assignin('base','path.psi',path.psi);

% fullpathname=strcat(pathname, filename);

% --- Executes on button press in iniciar_simulacion.
function iniciar_simulacion_Callback(hObject, eventdata, handles)
% hObject    handle to iniciar_simulacion (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
axes(handles.axes2);
DronImagen = imread('DronImagen.jpg');
imshow(DronImagen);

axes(handles.axes3);
UNIVA_Imagen = imread('UNIVA.JPG');
imshow(UNIVA_Imagen);
open('simulacion_total');

% --- Executes on button press in abrir_graficas.
function abrir_graficas_Callback(hObject, eventdata, handles)
% hObject    handle to abrir_graficas (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
resultados=evalin('base','resultados');
angulos_cmd_trayectoria=evalin('base','angulos_cmd_trayectoria');
scope_datos_trayectoria=evalin('base','scope_datos_trayectoria');
Scope1_Comandos_Comportamiento=evalin('base','Scope1_Comandos_Comportamiento');
C=evalin('base','C');
graficas(resultados,angulos_cmd_trayectoria,scope_datos_trayectoria,Scope1_Comandos_Comportamiento,C);

% --- Executes on button press in calcular_inercia.
function calcular_inercia_Callback(hObject, eventdata, handles)
% hObject    handle to calcular_inercia (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% hObject    handle to calcular_inercia (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%% Motores interpretaci�n de entrada
motor_m_g = str2num(get(handles.motores_editar_m, 'String'));
motor_dm_cm = str2num(get(handles.motores_editar_dm, 'String'));
motor_h_cm = str2num(get(handles.motores_editar_h, 'String'));
motor_r_cm = str2num(get(handles.motores_editar_r, 'String'));

%% ESC interpretaci�n de entrada
ESC_m_g = str2num(get(handles.ESC_editar_m, 'String'));
ESC_a_cm = str2num(get(handles.ESC_editar_a, 'String'));
ESC_b_cm = str2num(get(handles.ESC_editar_b, 'String'));
ESC_ds_cm = str2num(get(handles.ESC_editar_ds, 'String'));

%% Parte Central del Drone interpretaci�n de entrada
PCD_m_g = str2num(get(handles.PCD_editar_m, 'String'));
PCD_r_cm = str2num(get(handles.PCD_editar_r, 'String'));
PCD_H_cm = str2num(get(handles.PCD_editar_h, 'String'));

%% Brazos interpretaci�n de entrada
brazos_m_g = str2num(get(handles.brazos_editar_m, 'String'));
brazos_r_cm = str2num(get(handles.brazos_editar_r, 'String'));
brazos_L_cm = str2num(get(handles.brazos_editar_L, 'String'));
brazos_da_cm = str2num(get(handles.brazos_editar_da, 'String'));
%% Masa Total
masa = str2num(get(handles.masa_editar_m, 'String'));
%% Conversi�n al sistem Internacional de Medidas 
% Motores
motor_m = motor_m_g/1000;
motor_dm = motor_dm_cm/100;
motor_h = motor_h_cm/100;
motor_r = motor_r_cm/100;

% ESC
ESC_m = ESC_m_g/1000;
ESC_a = ESC_a_cm/100;
ESC_b = ESC_b_cm/100;
ESC_ds = ESC_ds_cm/100;

% PCD
PCD_m = PCD_m_g/1000;
PCD_r = PCD_r_cm/100;
PCD_H = PCD_H_cm/100;

% Brazos
brazos_m = brazos_m_g/1000;
brazos_r = brazos_r_cm/100;
brazos_L = brazos_L_cm/100;
brazos_da = brazos_da_cm/100;
% Masa Total
masa_k = masa/1000;
%% Momentos de inercia
% Este programa calcula los momentos de inercia del drone
%% Momentos de inercia Motores
% Jx1, Jy1, Jz1
inercia_motor_resultado_jx = (motor_m) * ((motor_r)^2) + (4/3)*(motor_m) * ((motor_h)^2) + 2*(motor_m)*((motor_dm)^2);
inercia_motor_resultado_jy = (motor_m) * ((motor_r)^2) + (4/3)*(motor_m) * ((motor_h)^2) + 2*(motor_m)*((motor_dm)^2);
inercia_motor_resultado_jz = 2*(motor_m)*((motor_r)^2) + (4)*(motor_m)*((motor_dm)^2);
set(handles.inercia_x_motor_valor, 'String', inercia_motor_resultado_jx);
set(handles.inercia_y_motor_valor, 'String', inercia_motor_resultado_jy);
set(handles.inercia_z_motor_valor, 'String', inercia_motor_resultado_jz);

%% Momentos de inercia ESC
% Jx2, Jy2, Jz2 
inercia_ESC_resultado_jx = ((ESC_m*(ESC_a^2))/6) + ((ESC_m*(ESC_b^2))/6) + (2*(ESC_m)*(ESC_ds^2));
inercia_ESC_resultado_jy = ((ESC_m*(ESC_a^2))/6) + ((ESC_m*(ESC_b^2))/6) + (2*(ESC_m)*(ESC_ds^2));
inercia_ESC_resultado_jz = ((ESC_m*(ESC_a^2 + ESC_b^2))/3) + 4*(ESC_m)*(ESC_ds^2);
set(handles.inercia_x__ESC_valor, 'String', inercia_ESC_resultado_jx);
set(handles.inercia_y__ESC_valor, 'String', inercia_ESC_resultado_jy);
set(handles.inercia_z__ESC_valor, 'String', inercia_ESC_resultado_jz);

%% Momentos de Inercia Parte Central del Drone
% Jx3, Jy3, Jz3 
inercia_PCD_resultado_jx = (1/4)*((PCD_m))*((PCD_r)^2) + (1/12)*((PCD_m))*((PCD_H)^2);
inercia_PCD_resultado_jy = (1/4)*((PCD_m))*((PCD_r)^2) + (1/12)*((PCD_m))*((PCD_H)^2);
inercia_PCD_resultado_jz = (1/2)*((PCD_m))*((PCD_r)^2);
set(handles.inercia_x__PCD_valor, 'String', inercia_PCD_resultado_jx);
set(handles.inercia_y__PCD_valor, 'String', inercia_PCD_resultado_jy);
set(handles.inercia_z__PCD_valor, 'String', inercia_PCD_resultado_jz);

% Momentos de inercia Brazos
% Jx4, Jy4, Jz4
inercia_brazos_resultado_jx = ((3/2)*(brazos_m)*(brazos_r^2)) + ((2/3)*(brazos_m)*(brazos_L^2)) + (2)*(brazos_m)*(brazos_da^2);
inercia_brazos_resultado_jy = ((3/2)*(brazos_m)*(brazos_r^2)) + ((2/3)*(brazos_m)*(brazos_L^2)) + (2)*(brazos_m)*(brazos_da^2);
inercia_brazos_resultado_jz = ((brazos_m)*((brazos_r)^2)) + (4/3)*((brazos_m)*((brazos_L)^2)) + (4)*(brazos_m)*(brazos_da^2);
set(handles.inercia_x__brazos_valor, 'String', inercia_brazos_resultado_jx);
set(handles.inercia_y__brazos_valor, 'String', inercia_brazos_resultado_jy);
set(handles.inercia_z__brazos_valor, 'String', inercia_brazos_resultado_jz);

%% Momentos de Inercia del drone completo
inercia_total_resultado_jx = inercia_motor_resultado_jx + inercia_ESC_resultado_jx + inercia_PCD_resultado_jx + inercia_brazos_resultado_jx;
inercia_total_resultado_jy = inercia_motor_resultado_jy + inercia_ESC_resultado_jy + inercia_PCD_resultado_jy + inercia_brazos_resultado_jy;
inercia_total_resultado_jz = inercia_motor_resultado_jz + inercia_ESC_resultado_jz + inercia_PCD_resultado_jz + inercia_brazos_resultado_jz;
set(handles.inercia_x_IT_valor, 'String', inercia_total_resultado_jx);
set(handles.inercia_y_IT_valor, 'String', inercia_total_resultado_jy);
set(handles.inercia_z_IT_valor, 'String', inercia_total_resultado_jz);


% --- Executes on button press in datos_dron_mat.
function datos_dron_mat_Callback(hObject, eventdata, handles)
% hObject    handle to datos_dron_mat (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%% ESTANDARIZAR DATOS A UN SISTEMA DE MEDICI�N
%% Motores interpretaci�n de entrada
motor_m_g = str2num(get(handles.motores_editar_m, 'String'));
motor_dm_cm = str2num(get(handles.motores_editar_dm, 'String'));
motor_h_cm = str2num(get(handles.motores_editar_h, 'String'));
motor_r_cm = str2num(get(handles.motores_editar_r, 'String'));

%% ESC interpretaci�n de entrada
ESC_m_g = str2num(get(handles.ESC_editar_m, 'String'));
ESC_a_cm = str2num(get(handles.ESC_editar_a, 'String'));
ESC_b_cm = str2num(get(handles.ESC_editar_b, 'String'));
ESC_ds_cm = str2num(get(handles.ESC_editar_ds, 'String'));

%% Parte Central del Drone interpretaci�n de entrada
PCD_m_g = str2num(get(handles.PCD_editar_m, 'String'));
PCD_r_cm = str2num(get(handles.PCD_editar_r, 'String'));
PCD_H_cm = str2num(get(handles.PCD_editar_h, 'String'));

%% Brazos interpretaci�n de entrada
brazos_m_g = str2num(get(handles.brazos_editar_m, 'String'));
brazos_r_cm = str2num(get(handles.brazos_editar_r, 'String'));
brazos_L_cm = str2num(get(handles.brazos_editar_L, 'String'));
brazos_da_cm = str2num(get(handles.brazos_editar_da, 'String'));
%% Masa Total
masa = str2num(get(handles.masa_editar_m, 'String'));
%% Conversi�n al sistem Internacional de Medidas 
% Motores
motor_m = motor_m_g/1000;
motor_dm = motor_dm_cm/100;
motor_h = motor_h_cm/100;
motor_r = motor_r_cm/100;

% ESC
ESC_m = ESC_m_g/1000;
ESC_a = ESC_a_cm/100;
ESC_b = ESC_b_cm/100;
ESC_ds = ESC_ds_cm/100;

% PCD
PCD_m = PCD_m_g/1000;
PCD_r = PCD_r_cm/100;
PCD_H = PCD_H_cm/100;

% Brazos
brazos_m = brazos_m_g/1000;
brazos_r = brazos_r_cm/100;
brazos_L = brazos_L_cm/100;
brazos_da = brazos_da_cm/100;
% Masa Total
masa_k = masa/1000;
%% Momentos de inercia
% Este programa calcula los momentos de inercia del drone
%% Momentos de inercia Motores
% Jx1, Jy1, Jz1
Jx1 = (motor_m) * ((motor_r)^2) + (4/3)*(motor_m) * ((motor_h)^2) + 2*(motor_m)*((motor_dm)^2);
Jy1 = (motor_m) * ((motor_r)^2) + (4/3)*(motor_m) * ((motor_h)^2) + 2*(motor_m)*((motor_dm)^2);
Jz1 = 2*(motor_m)*((motor_r)^2) + (4)*(motor_m)*((motor_dm)^2);
%% Momentos de inercia ESC
% Jx2, Jy2, Jz2 
Jx2 = ((ESC_m*(ESC_a^2))/6) + ((ESC_m*(ESC_b^2))/6) + (2*(ESC_m)*(ESC_ds^2));
Jy2 = ((ESC_m*(ESC_a^2))/6) + ((ESC_m*(ESC_b^2))/6) + (2*(ESC_m)*(ESC_ds^2));
Jz2 = ((ESC_m*(ESC_a^2 + ESC_b^2))/3) + 4*(ESC_m)*(ESC_ds^2);
%% Momentos de Inercia Parte Central del Drone
% Jx3, Jy3, Jz3 
Jx3 = (1/4)*((PCD_m))*((PCD_r)^2) + (1/12)*((PCD_m))*((PCD_H)^2);
Jy3 = (1/4)*((PCD_m))*((PCD_r)^2) + (1/12)*((PCD_m))*((PCD_H)^2);
Jz3 = (1/2)*((PCD_m))*((PCD_r)^2);
%% Momentos de inercia Brazos
% Jx4, Jy4, Jz4
Jx4 = ((3/2)*(brazos_m)*(brazos_r^2)) + ((2/3)*(brazos_m)*(brazos_L^2)) + (2)*(brazos_m)*(brazos_da^2);
Jy4 = ((3/2)*(brazos_m)*(brazos_r^2)) + ((2/3)*(brazos_m)*(brazos_L^2)) + (2)*(brazos_m)*(brazos_da^2);
Jz4 = ((brazos_m)*((brazos_r)^2)) + (4/3)*((brazos_m)*((brazos_L)^2)) + (4)*(brazos_m)*(brazos_da^2);
%% Momentos de Inercia del drone completo
Jx = Jx1 + Jx2 + Jx3 + Jx4;
Jy = Jy1 + Jy2 + Jy3 + Jy4;
Jz = Jz1 + Jz2 + Jz3 + Jz4;
%Jb
Jb = [Jx 0 0; 0 Jy 0; 0 0 Jz];
Jbinv = [1/Jx 0 0; 0 1/Jy 0; 0 0 1/Jz];

%% Velocidad Angular
P = str2num(get(handles.VelocidadAngular_editar_P, 'String'));
Q = str2num(get(handles.VelocidadAngular_editar_Q, 'String'));
R = str2num(get(handles.VelocidadAngular_editar_R, 'String'));
%% �ngulos Euler
Phi = str2num(get(handles.AnguloEuler_editar_phi, 'String'));
The = str2num(get(handles.AnguloEuler_editar_the, 'String'));
Psi = str2num(get(handles.AnguloEuler_editar_psi, 'String'));
%% Velocidad Lineal
U = str2num(get(handles.VelocidadLineal_editar_U, 'String'));
V = str2num(get(handles.VelocidadLineal_editar_V, 'String'));
W = str2num(get(handles.VelocidadLineal_editar_W, 'String'));
%% Posici�n con Respecto a la Tierra
X = str2num(get(handles.posicion_editar_X, 'String'));
Y = str2num(get(handles.posicion_editar_Y, 'String'));
Z = str2num(get(handles.posicion_editar_Z, 'String'));
%% Velocidad de los motores
w1 = str2num(get(handles.VelocidadMotor_editar_w1, 'String'));
w2 = str2num(get(handles.VelocidadMotor_editar_w2, 'String'));
w3 = str2num(get(handles.VelocidadMotor_editar_w3, 'String'));
w4 = str2num(get(handles.VelocidadMotor_editar_w4, 'String'));
%% Coeficientes del motor
CQ = str2num(get(handles.motor_CQ_valor, 'String'));
CR = str2num(get(handles.motor_CR_valor, 'String'));
b = str2num(get(handles.motor_b_valor, 'String'));
CT = str2num(get(handles.motor_CT_valor, 'String'));

g=str2num(get(handles.gravedad_editar_g, 'String'));
% dctq = str2num(get(handles.dctq_editar, 'String'));

% PID roll
roll_kp=str2num(get(handles.PID_editar_Roll_kp, 'String'));
roll_kip=str2num(get(handles.PID_editar_Roll_kip, 'String'));
roll_kd=str2num(get(handles.PID_editar_Roll_kd, 'String'));

% PID pitch
pitch_kpt=str2num(get(handles.PID_editar_Pitch_kpt, 'String'));
pitch_kit=str2num(get(handles.PID_editar_Pitch_kit, 'String'));
pitch_kdt=str2num(get(handles.PID_editar_Pitch_kdt, 'String'));

% PID yaw
yaw_kps=str2num(get(handles.PID_editar_yaw_kps, 'String'));
yaw_kis=str2num(get(handles.PID_editar_yaw_kis, 'String'));
yaw_kds=str2num(get(handles.PID_editar_yaw_kds, 'String'));

% PID Z
z_kps=str2num(get(handles.PID_editar_z_kpz, 'String'));
z_kiz=str2num(get(handles.PID_editar_z_kiz, 'String'));
z_kdz=str2num(get(handles.PID_editar_z_kdz, 'String'));

    ModeloDron = struct('motor_m',(motor_m),'motor_dm',(motor_dm),'motor_h',(motor_h),'motor_r',(motor_r),...
'ESC_m',(ESC_m),'ESC_a',(ESC_a),'ESC_b',(ESC_b),'ESC_ds',(ESC_ds),...
'PCD_m',(PCD_m),'PCD_r',(PCD_r),'PCD_H',(PCD_H),...
'brazos_m',(brazos_m),'brazos_r',(brazos_r),'brazos_L',(brazos_L),'brazos_da',(brazos_da),'masa',(masa_k),...
'Jx',(Jx),'Jy',(Jy),'Jz',(Jz),'Jb',(Jb),'Jbinv',(Jbinv),...
'CQ',(CQ),'CR',(CR),'b',(b),'CT',(CT),'g',(g));


CI = struct('P',(P),'Q',(Q),'R',(R),...
    'Phi',(Phi),'The',(The),'Psi',(Psi),...
    'U',(U),'V',(V),'W',(W),...
    'X',(X),'Y',(Y),'Z',(Z),...
    'w1',(w1),'w2',(w2),'w3',(w3),'w4',(w4));

PID =struct('roll_kp',(roll_kp),'roll_kip',(roll_kip),'roll_kd',(roll_kd),...
'pitch_kpt',(pitch_kpt),'pitch_kit',(pitch_kit),'pitch_kdt',(pitch_kdt),...
'yaw_kps',(yaw_kps),'yaw_kis',(yaw_kis),'yaw_kds',(yaw_kds),...
'z_kps',(z_kps),'z_kiz',(z_kiz),'z_kdz',(z_kdz));

assignin('base','ModeloDron',ModeloDron);
assignin('base','CI',CI);
assignin('base','PID',PID);
uisave('ModeloDron','ModeloDron');
uisave('CI','CI');
uisave('PID','PID');

guidata(hObject, handles);

function initialize_gui(fig_handle, handles, isreset)
% If the metricdata field is present and the reset flag is false, it means
% we are we are just re-initializing a GUI by calling it from the cmd line
% while it is up. So, bail out as we dont want to reset the data.
if isfield(handles, 'metricdata') && ~isreset
    return;
end

handles.metricdata.motores_editar_m= 0;
set(handles.motores_editar_m, 'String', handles.metricdata.motores_editar_m);

handles.metricdata.motores_editar_dm= 0;
set(handles.motores_editar_dm, 'String', handles.metricdata.motores_editar_dm);

handles.metricdata.motores_editar_h= 0;
set(handles.motores_editar_h, 'String', handles.metricdata.motores_editar_h);

handles.metricdata.motores_editar_r= 0;
set(handles.motores_editar_r, 'String', handles.metricdata.motores_editar_r);

handles.metricdata.ESC_editar_m= 0;
set(handles.ESC_editar_m, 'String', handles.metricdata.ESC_editar_m);

handles.metricdata.ESC_editar_a= 0;
set(handles.ESC_editar_a, 'String', handles.metricdata.ESC_editar_a);

handles.metricdata.ESC_editar_b= 0;
set(handles.ESC_editar_b, 'String', handles.metricdata.ESC_editar_b);

handles.metricdata.ESC_editar_ds= 0;
set(handles.ESC_editar_ds, 'String', handles.metricdata.ESC_editar_ds);

handles.metricdata.PCD_editar_m= 0;
set(handles.PCD_editar_m, 'String', handles.metricdata.PCD_editar_m);

handles.metricdata.PCD_editar_r= 0;
set(handles.PCD_editar_r, 'String', handles.metricdata.PCD_editar_r);

handles.metricdata.PCD_editar_h= 0;
set(handles.PCD_editar_h, 'String', handles.metricdata.PCD_editar_h);

handles.metricdata.brazos_editar_m= 0;
set(handles.brazos_editar_m, 'String', handles.metricdata.brazos_editar_m);

handles.metricdata.brazos_editar_r= 0;
set(handles.brazos_editar_r, 'String', handles.metricdata.brazos_editar_r);

handles.metricdata.brazos_editar_L= 0;
set(handles.brazos_editar_L, 'String', handles.metricdata.brazos_editar_L);

handles.metricdata.brazos_editar_da= 0;
set(handles.brazos_editar_da, 'String', handles.metricdata.brazos_editar_da);

handles.metricdata.motor_CQ_editar_final= 0;
set(handles.motor_CQ_editar_final, 'String', handles.metricdata.motor_CQ_editar_final);

handles.metricdata.motor_CR_editar_final= 0;
set(handles.motor_CR_editar_final, 'String', handles.metricdata.motor_CR_editar_final);

handles.metricdata.motor_b_editar_final= 0;
set(handles.motor_b_editar_final, 'String', handles.metricdata.motor_b_editar_final);

handles.metricdata.motor_b_editar_final= 0;
set(handles.motor_b_editar_final, 'String', handles.metricdata.motor_b_editar_final);

handles.metricdata.motor_CT_editar_final= 0;
set(handles.motor_CT_editar_final, 'String', handles.metricdata.motor_CT_editar_final);

handles.metricdata.masa_editar_m= 0;
set(handles.masa_editar_m, 'String', handles.metricdata.masa_editar_m);

handles.metricdata.masa_editar_m= 0;
set(handles.masa_editar_m, 'String', handles.metricdata.masa_editar_m);

handles.metricdata.gravedad_editar_g= 0;
set(handles.gravedad_editar_g, 'String', handles.metricdata.gravedad_editar_g);

handles.metricdata.VelocidadAngular_editar_P= 0;
set(handles.VelocidadAngular_editar_P, 'String', handles.metricdata.VelocidadAngular_editar_P);

handles.metricdata.VelocidadAngular_editar_Q= 0;
set(handles.VelocidadAngular_editar_Q, 'String', handles.metricdata.VelocidadAngular_editar_Q);

handles.metricdata.VelocidadAngular_editar_R= 0;
set(handles.VelocidadAngular_editar_R, 'String', handles.metricdata.VelocidadAngular_editar_R);

handles.metricdata.AnguloEuler_editar_phi= 0;
set(handles.AnguloEuler_editar_phi, 'String', handles.metricdata.AnguloEuler_editar_phi);

handles.metricdata.AnguloEuler_editar_phi= 0;
set(handles.AnguloEuler_editar_phi, 'String', handles.metricdata.AnguloEuler_editar_phi);

handles.metricdata.AnguloEuler_editar_the= 0;
set(handles.AnguloEuler_editar_the, 'String', handles.metricdata.AnguloEuler_editar_the);

handles.metricdata.AnguloEuler_editar_psi= 0;
set(handles.AnguloEuler_editar_psi, 'String', handles.metricdata.AnguloEuler_editar_psi);

handles.metricdata.posicion_editar_X= 0;
set(handles.posicion_editar_X, 'String', handles.metricdata.posicion_editar_X);

handles.metricdata.posicion_editar_Y= 0;
set(handles.posicion_editar_Y, 'String', handles.metricdata.posicion_editar_Y)

handles.metricdata.posicion_editar_Z= 0;
set(handles.posicion_editar_Z, 'String', handles.metricdata.posicion_editar_Z)

handles.metricdata.VelocidadLineal_editar_U= 0;
set(handles.VelocidadLineal_editar_U, 'String', handles.metricdata.VelocidadLineal_editar_U)

handles.metricdata.VelocidadLineal_editar_V= 0;
set(handles.VelocidadLineal_editar_V, 'String', handles.metricdata.VelocidadLineal_editar_V)

handles.metricdata.VelocidadLineal_editar_W= 0;
set(handles.VelocidadLineal_editar_W, 'String', handles.metricdata.VelocidadLineal_editar_W)

handles.metricdata.VelocidadMotor_editar_w1= 0;
set(handles.VelocidadMotor_editar_w1, 'String', handles.metricdata.VelocidadMotor_editar_w1)

handles.metricdata.VelocidadMotor_editar_w2= 0;
set(handles.VelocidadMotor_editar_w2, 'String', handles.metricdata.VelocidadMotor_editar_w2)

handles.metricdata.VelocidadMotor_editar_w3= 0;
set(handles.VelocidadMotor_editar_w3, 'String', handles.metricdata.VelocidadMotor_editar_w3)

handles.metricdata.VelocidadMotor_editar_w4= 0;
set(handles.VelocidadMotor_editar_w4, 'String', handles.metricdata.VelocidadMotor_editar_w4)

set(handles.texto_datos_precargados, 'String', 'DATOS PRECARGADOS');
set(handles.motor_CQ_valor, 'String', 0);
set(handles.motor_CR_valor, 'String', 0);
set(handles.motor_b_valor, 'String', 0);
set(handles.motor_CT_valor, 'String', 0);
set(handles.inercia_x_motor_valor, 'String', 0);
set(handles.inercia_y_motor_valor, 'String', 0);
set(handles.inercia_z_motor_valor, 'String', 0);
set(handles.inercia_x__ESC_valor, 'String', 0);
set(handles.inercia_y__ESC_valor, 'String', 0);
set(handles.inercia_z__ESC_valor, 'String', 0);
set(handles.inercia_x__PCD_valor, 'String', 0);
set(handles.inercia_y__PCD_valor, 'String', 0);
set(handles.inercia_z__PCD_valor, 'String', 0);
set(handles.inercia_x__brazos_valor, 'String', 0);
set(handles.inercia_y__brazos_valor, 'String', 0);
set(handles.inercia_z__brazos_valor, 'String', 0);
set(handles.inercia_x_IT_valor, 'String', 0);
set(handles.inercia_y_IT_valor, 'String', 0);
set(handles.inercia_z_IT_valor, 'String', 0);

% Update handles structure
guidata(handles.figure1, handles);


% --- Executes on button press in animacion.
function animacion_Callback(hObject, eventdata, handles)
% hObject    handle to animacion (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
resultados=evalin('base','resultados');
angulos_cmd_trayectoria=evalin('base','angulos_cmd_trayectoria');
scope_datos_trayectoria=evalin('base','scope_datos_trayectoria');
Scope1_Comandos_Comportamiento=evalin('base','Scope1_Comandos_Comportamiento');
C=evalin('base','C');
iniciar_animacion(resultados,angulos_cmd_trayectoria,scope_datos_trayectoria,Scope1_Comandos_Comportamiento,C);

function CargarDatosExcel_CreateFcn(hObject, eventdata, handles)
% hObject    handle to gravedad_editar_total (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
