%% Generaci?n de trayectoria
X=[0 -4 -4 -3 -3 -2 -2 -1 -1 0 0 1 2 3 4 5 6]; % metros
Y=[0  3  0  0  3  0  3  0  3 3 0 3 0 3 0 3 0]; % metros
Z=[0  3  4  5  6  7  8  9  10 11 12 13 14 15 16 17 18]; % metros
t=[0  5  10 15 20 25 30 35 40,50,55,60,65,70,75,80,85]; % segundos
angulo=[0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0]; % radians
path.x = timeseries(X,t);
path.y = timeseries(Y,t);
path.z = timeseries(Z,t);
path.psi = timeseries(angulo,t);