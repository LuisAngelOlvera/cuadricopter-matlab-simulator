% Generación trayectoria TRIANGULO
X=[0 0  3  0  0   3 0]; % metros
Y=[0 0  0  3  0   0 0]; % metros
Z=[0 3  6  9 12 12 12]; % metros
t=[0 5 10 15 20 25 30]; % segundos
angulo=[0,0,0,0,0,0,0]; % radians
path.x = timeseries(X,t);
path.y = timeseries(Y,t);
path.z = timeseries(Z,t);
path.psi = timeseries(angulo,t);