function [CQ] = calcular_CQ(segundos, acelerador, RPM, medicion_gramos)
L=0.20;
CorteMin=min(acelerador);
CorteMax=max(acelerador);

aceleradorU = unique(acelerador(acelerador~=0));
medicion_gramosU = unique(medicion_gramos(medicion_gramos~=0));

longitudMG = length(medicion_gramosU);
longitudAU = length(aceleradorU);

if longitudMG ~= longitudAU
    msgbox({['Vector de las mediciones de gramos: (' num2str(longitudMG) ')'];
        [' y los valores �nicos del vector aceleradorTU (' num2str(longitudAU) ')']; ...
       ' No son iguales! Revisar los valores de entrada!'}');
    return
end

aceleradorUF = aceleradorU(aceleradorU>=CorteMin & aceleradorU<=CorteMax);
medicion_gramosUF = medicion_gramosU(aceleradorU>=CorteMin & aceleradorU<=CorteMax);

RPMpromedios100 = zeros(length(aceleradorUF),1);

for i = 1:length(aceleradorUF)% For all of the unique and filtered throttle settings:
    RPMUF = RPM(acelerador==aceleradorUF(i));  % get RPM values cooresponding to unique/filtered Throttle value
    RPMpromedios100(i,1) = mean(RPMUF); % Take the average RPM value from these filtered data points
end
RPMsq = RPMpromedios100.^2;

g = 9.81; % Gravedad (m/s^2)
TorqueNewtonsMetro = g*L*medicion_gramosU/(1000*39.3701); % Torque in N*m (Conversion: (g-->kg and in-->m))
CQ = RPMsq\TorqueNewtonsMetro; % This is a technique for forcing a zero intercept using matrix division techniques
CQp = [CQ;0];

% Plot the linear relationship and display calculated CQ value
figure
plot(RPMsq, TorqueNewtonsMetro, '*', RPMsq, polyval(CQp, RPMsq))
xlabel('RPM^2 (rev^2/min^2)','FontSize',12)
ylabel('Torque (N*m)','FontSize',12)
title({['Arreglo Lineal del Torque vs. RPM^2'];...
    [' ']; ['CQ: ' num2str(CQ) ' (N*m/RPM^2)']},'FontSize',14)
grid on
end